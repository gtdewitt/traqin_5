/*
*
*
* CONTACTDETAIL CONTROLLER
*
*
*/

    var ContactDetail = {

       friendId         : null,
       gMap             : null,
       map              : null,
       mapCenter        : null,
       contactData      : null,
       currentPosition  : null,

       init: function() {
     //       alert("I'm inside Jim!" + venue['lat'] + venue['lng']);
          var me = this;

            me.contactData  = venue;
            me.mapCenter    = new google.maps.LatLng (venue['lat'], venue['lng']);
            me.initMap();
            me.outputDetails();
            me.configureButtons();
            me.outputDistance();

    /*      FriendsWithBeerDB.runQuery("select friend.*, beer.name beername from friend LEFT JOIN beer on friend.beerid = beer.id where friend.id = " + this.friendId, function(records) {
            me.contactData = records[0];
            me.mapCenter = new google.maps.LatLng (records[0].lat, records[0].lng);
            me.initMap();
            me.outputDetails();
            me.configureButtons();
            me.outputDistance();
          }); */
       },

       configureButtons: function() {

       //    alert("Im in configureButtons");

          $("#btnPhone").bind("tap", function(e) {
              var telLink = "tel:" + ContactDetail.contactData.phone;
              location.href=telLink;
            //  Notifications.init();
          });

          $("#btnMail").bind("tap", function(e) {
              var mailLink = "mailto:{0}?subject={1}&body={2}";
              mailLink = mailLink.format(
                  ContactDetail.contactData.email,
                  escape("Can I come over now?"),
                  escape("Hey good buddy, can I come over? Need to chat with you about some stuff!")
              );
              location.href = mailLink;
          });

          $("#btnNav").bind("tap", function(e) {
              if( /Android/i.test(navigator.userAgent) ) {

                 var mapsUrl = "http://maps.google.com/maps?daddr={0},{1}&saddr={2},{3}";
                 var url = mapsUrl.format(
                      ContactDetail.contactData.lat,
                      ContactDetail.contactData.lng,
                      ContactDetail.currentPosition.lat(),
                      ContactDetail.currentPosition.lng()
                 );
                 window.open(url, '_blank', 'location=yes');

                  // clickLink(mapsUrl);

                  /*
                  window.plugins.webintent.startActivity(
                     {
                      action: window.plugins.webintent.ACTION_VIEW,
                      url: 'geo:0,0?q=1400 16th st NW 20036'
                     },
                     function() {},
                     function() {alert('Failed to open URL via Android Intent')}
                   );

                  return;
                  */

              } else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {

                  var mapsUrl = "maps://?daddr={0},{1}&saddr={2},{3}";

                   var url = mapsUrl.format(
                      ContactDetail.contactData.lat,
                      ContactDetail.contactData.lng,
                      ContactDetail.currentPosition.lat(),
                      ContactDetail.currentPosition.lng()
                    );

                  location.href = url;
              } else {
                alert("Not supported");
              }

          });

         $("#btnTwitter").bind("tap", function(e) {
             Notifications.init();

      /*      var html = <a class="twitter-timeline"  href="https://twitter.com/gtdewitt" data-widget-id="547106134796951553">Tweets by @gtdewitt</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)      [0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

             $("#notifications").html(html) ;  */
          });



       },

   /*    outputDetails: function() {
         me.contactData = venue;
          var tplContact = $("#tplContact").html();
          var html = tplContact.format(
            this.contactData.firstName,
            this.contactData.lastName,
            this.contactData.address,
            this.contactData.beername
          );
          $("#contactdetails").html(html);
       }, */

       outputDetails: function() {
        //   alert("I'm in outputDetails " + venue['lat']);
           var me = this;
          var tplContact = $("#tplContact").html();

          var html = tplContact.format(
            venue.name,
            venue.fAddress,
            venue.phone,
            venue.name
          );
           console.log(tplContact);
        //   html = "<div class=\"contactname\">"+venue['name']+"</div><div class=\"contactaddress\">"+venue['address']+"</div><div class=\"contactphone\">"+venue['phone']+"</div><div class=\"contactdistance\">"+venue['name']+" is only <span id=\"contactdistance\"></span> away!</div>";

           console.log(html);
           $("#contactdetails").html(html);
          // me.outputDistance();
       },

      outputDistance: function() {
          var me = this;
     //     alert("Im in aoutput Distances");
     //     alert("me mapCenter is " + me.mapCenter);
      //    debugger;
           navigator.geolocation.getCurrentPosition(

             function(position) {
                ContactDetail.currentPosition = new google.maps.LatLng(
                    position.coords.latitude,
                    position.coords.longitude
                );
                 miles = calcDistance(
                     me.mapCenter.lat(),
                     me.mapCenter.lng(),
                     position.coords.latitude,
                     position.coords.longitude
                );
             //   alert("miles = " + miles);
                $("#contactdistance").html(miles + " miles ");

             },

            function (error) {
               alert("An error occurred");
            }
           )
       },

       initMap: function() {
         var $mapContainer  = $("#map2"),
             me             = this;
         this.gMap = $mapContainer.gmap({
            center: this.mapCenter,
            zoom: 16,
            callback: function(map) {
                me.map = map;
                var trafficLayer = new google.maps.TrafficLayer();
                trafficLayer.setMap(map);
                var panoramioLayer = new google.maps.panoramio.PanoramioLayer();
                panoramioLayer.setMap(map);
              //  ctMarker();

               this.addMarker({
                    position: map.getCenter(),
                    'icon': 'Logos/ct_marker_shdw_64.png',
                    'title': venue.name
                });
             }
          });

          $mapContainer.height ($("body").height() - 200);
          setTimeout("$('#map2').gmap('refresh')",500);
      },

       getFormData: function() {
           alert('Need to develope this ContactDetail.getFormData function')
    }
}

/*
*
*
* CONTACTFORM CONTROLLER
*
*
*/

 /*  var ContactForm = {

     friendId: null,

     init: function() {

       this.initialized=true;
       this.populateBeerField();
       this.initValidation();
       this.loadRecord();
       var me = this;

       $("#btnSave").bind("tap", function(e) {
          me.submitForm();
       });

       $("#address").bind("change", this.geoCode);
       $("#zipcode").bind("change", this.geoCode);
     },

     loadRecord: function() {
        if (this.friendId != null) {
          FriendsWithBeerDB.runQuery("select * from friend where id = " + this.friendId, function(records) {
            var rec = records[0];
            for (var i in rec) {
              $("#" + i).val(rec[i]);
            }

            $("#beerId").selectmenu("refresh", true);
          });
        }
     },

     populateBeerField: function() {
       var beerField = $("select#beerId");
       // beerField.empty();
       for (var i=0; i< App.beers.length; i++) {
        beerField.append(
            '<option value="{0}">{1}</option>'.format(App.beers[i].id,App.beers[i].name)
        )
       }
       beerField.selectmenu("refresh", true);
     },

     initValidation: function() {
      var form = $("form#contactform");
      form.validate({
        submitHandler: function(form) {
            // $(form).ajaxSubmit();
            alert(2);
        },
        rules: {
         lastName: {
           required: true,
           rangelength: [2,20]
         },
         firstName: {
          required: true
         },
         address: {
          required: true
         },
         zipcode: {
          required: true,
          digits: true
         }
      }
     });
    },

    submitForm: function() {
       var form =  $("form#contactform");
       var validator = form.validate();
       validator.form();
       if (validator.numberOfInvalids() == 0) {

           var fields = form.serializeArray();

           fields[fields.length] = {
             name: 'lat',
             value: $('#lat').val()
           };

           fields[fields.length] = {
             name: 'lng',
             value: $('#lng').val()
           };

           FriendsWithBeerDB.writeRecord('friend', this.friendId, fields, function() {
                alert("Record Saved");
                $.mobile.changePage('contacts.html')
           });
       }
    },

    geoCode: function() {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            address : $('#address').val() + " " + $('#zipcode').val()
         }, function(results,status) {
            if(status != google.maps.GeocoderStatus.OK) {
		     alert("Address not found");
             $("#lat").val('');
             $("#lng").val('');
	       } else {
		     $("#lat").val(results[0].geometry.location.lat());
             $("#lng").val(results[0].geometry.location.lng());
	       }
         });
     }

   }  */


/*
*
*
* HELPER FUNCTIONS
*
*
*/

    String.prototype.format = function () {
      var args = arguments;
      return this.replace(/\{(\d+)\}/g, function (m, n) { return args[n]; });
    };

    calcDistance = function(lat1,lng1,lat2,lng2) {

         var R = 3959; // use 3959 for miles or 6371 for km
         var dLat = (lat2-lat1) * Math.PI / 180 ;
         var dLon = (lng2-lng1)* Math.PI / 180;
         var lat1 = lat1 * Math.PI / 180;
         var lat2 = lat2 * Math.PI / 180;

         var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
         var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

         return (R * c).toFixed(2);
    };

  /*  myMarkers = function(){
            var myLatlng = new google.maps.LatLng(37.775, -122.4183333);
            var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: venue.name,
            icon: "Logos/ct_marker_shdw_64.png"
            });
    };

    ctMarkerCustom = function(){
        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          icon: iconBase + 'Logos/ct_marker_shdw_64.png'
        });

    };

     ctMarter = function(){
        $("#map2").gmap('addMarker', {
     //   "id": "marker-"+m.id,
        'position': new google.maps.LatLng(venue.lat, venue.lon),
        'bounds':true,
        'icon': 'Logos/ct_marker_shdw_64.png'
        });
      } */

      /*  Market infowindow example
            Marker marker = myMap.addMarker(new MarkerOptions()
             .position(latLng)
             .title("Title")
             .snippet("Snippet")
             .icon(BitmapDescriptorFactory
             .fromResource(R.drawable.marker)));

            marker.showInfoWindow();  */



/*
*
*
* PUSH NOTIFICATION FUNCTIONS
*
*
*/

   var Notifications = {

       init: function() {
           Notifications.message();
       },

        message: function() {

       /*    var notice = '<div data-role="header' +
                            '<h1>I\'m A Dialog Box!</h1>' +
                          '</div>' +
                          '<div data-role="main" class="ui-content">' +
                            '<p>The dialog box is different from a normal page, it is displayed on top of the current page and it will not span the entire width of the page. The dialog has also an icon of "X" in the header to close the box.</p>' +
                            '<a href="http://www.twitter.com">Go to Twitter</a>' +
                          '</div>' +
                          '<div data-role="footer">' +
                            '<h1>Footer Text In Dialog</h1>' +
                          '</div>' ;  */
            $( "#notify" ).toggle();

         /*   console.log("This is notice: "+notice);
             $("#notify").html(notice); */
          //   $("#notify").fadeIn();
       },

   }

 /*  var SidebarOn = {
        $( "#mapMarker" ).click(function() {
          var color = $( this ).css( "background-color" );
          $( "#result" ).html( "That div is <span style='color:" +
            color + ";'>" + color + "</span>." );
        });
    } */
