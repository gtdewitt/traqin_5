/*jslint sloppy:true, plusplus: true, sub: true, vars: true, white: true */
/*jshint sub: true */
/*global $, Math, locate.calcDirection, document, google, navigator, onDeviceReady, onGeoError, onGeoSuccess, parseInt, locate.relativePosition, cityMapper.setup,  window */

var FOURSQUARE_CLIENT_ID        = "LJ2FHJVVTMTTH4RVK4WI3UQI0RG4LAJHPWCDJOQJ3D1ZWREA",
    FOURSQUARE_CLIENT_SECRET    = "U1BCHMYQSUJDDTSTASKEPN00PFAD4AG5MU5S45EPOGO3PPXI",
    markersArray                = [], bounds, map, watchGeoID, watchCompassID, watchAccelerometerID, pin,
    currentOffer                = {},
    currentQueue                = {},
    myLat                       = 0,
    myLng                       = 0,
    myEta                       = null,
    myProfile                   = {},
    myDevice                    = {},
    myOffer                     = {},
    myOffers                    = [],
    myQueueReq                  = {},
    myQueue                     = {},
    myQueues                    = [],
    myCheckin                   = {},
    myCheckins                  = [],
    myProfObj                   = { 'myProfile': null, 'myDevice': null, 'myOffers': [], 'myQueues': [] } ,  
    myPosObj                    = {},

    bearing                     = 0,
    distance                    = 0,
    dataStatus                  = 0,
    geoTimer                    = 1000,
    q                           = "casino",
    ctMarker                    = "Logos/ct_marker_shdw_64.png",
    ctMarkerSm                  = "Logos/ct_marker_shdw_32.png",
    myUsername                  = null,
    myPassword                  = null,
    myFirstName                 = null,
    myLastName                  = null,
    myCell                      = null,
    myEmail                     = null,
    myMac                       = null,
    myUuid                      = null,
    myCordova                   = null,
    myModel                     = null,
    myPlatform                  = null,
    myVersion                   = null,
    myManufacturer              = null,
    myVirtual                   = null,
    mySerial                    = null,
    offerSeed                   = null,  // store offer token seed for checkin verification
    offerAcceptToken            = null,  // $2a$08$YDddMCII9o2lka4OM8ysUO6mMZG3FIVEKFxk6S3N3iWwkBun8iqcy
    offerAcceptTokenKey         = null,  // $2a$08$
    offerAcceptTokenHash        = null,  // YDddMCII9o2lka4OM8ysUO6mMZG3FIVEKFxk6S3N3iWwkBun8iqcy
    loggedIn                    = false, // activate accelerometer tilt switch when logged in
    offerView                   = false, // render accelerometer tilt switch inactive when in offer details view
 //  qActive                     = false,
 //  sendEta                     = false,
    routeMap                    = null,
    directionsDisplay           = null,
    searchType                  = null,
    mapTimerHandle, trafficLayer;

// setup map and listen to deviceready
$( document ).ready(function() {
    document.addEventListener("deviceready", onDeviceReady, false);
});


// start device compass, accelerometer and geolocation after deviceready        
function onDeviceReady() {
    if( window.Cordova && navigator.splashscreen ) {     // Cordova API detected
        navigator.splashscreen.hide() ;                 // hide splash screen
        
        
    
        
        
     //   window.Cordova.ToastyPlugin.prototype.initialize();
     //   window.plugins.toastyPlugin.initialize(function () {
       window.plugins.toastyPlugin.initialize(function () { 
           console.log('success!');
       }, function (err) {
           console.error('error loading:', err);
       });
        


            /*  $("button").click(function(){
                $("p").hide();
              }); */
    }
    // get device id information
    user.getDeviceInfo();
   // appPage.listView();                                 
    // initialize map in canvas
     cityMapper.init();
    // start cordova device sensors
     sensor.accelerometer.start();  //     offerView = true; initially until logged in
     sensor.compass.start();
     sensor.geolocation.watchPos.start();
    // start in list view
    appPage.loginView(); 
    login.init();
    
    $(document).on("click", ".showVenue", function (e) {  // show venue details using offerId from listview item or AR spot click
        e.preventDefault();
        var dataId = $(this).attr('data-id');
     //   alert('data-id in .showVenue click: ' + dataId);

        currentOffer = pin[dataId];   // Property and offer data currently selected
        myOffers.push(currentOffer);        // this pushes the seleted offer to the nested myOffers[] stack
    //    alert('myOffers[0].name in user.offer is: '+myOffers[0].name);
        
        appPage.venueDetailsView(currentOffer);    //  pin[dataId]

     //   VenueDetail.init(pin[dataId]);  <!-- Data fill the contact details page -->
    });
    
 /*  $(document).on("click", "#btnAccept", function (e) {
        e.preventDefault();
        appPage.queueView();
     //   queue.doToggle();
     //   var dataId = $(this).attr('data-id');
    });   */
    
    $(document).on("click", "#btnCheckin", function (e) {   
        e.preventDefault();
        appPage.connectView();
     //   queue.doToggle();
     //   var dataId = $(this).attr('data-id');
    });  
    
        $(document).on("click", "#btnUnregWallet", function (e) {   
        e.preventDefault();
        appPage.connectView();
     //   queue.doToggle();
     //   var dataId = $(this).attr('data-id');
    }); 
    
 /*   $(document).on("click", "#btnRegister", function (e) {   
        e.preventDefault();
        logout.removeLocalStorage();
        appPage.registerView(); 
    });   */

    // to be used for register/login dialog
/*    $( 'span.register' ).click ( function () { 
      $( "#dialog_register" ).dialog ( {
            buttons: {
                  Cancel: function () { $( this ).dialog ( "close" ); },
                  Confirm: function () {
                        $.ajax ({ ... });
                        $( this ).dialog ( "close" );
                  }
            }
      } );
    } );  */
    
    qEventFlow.init();  // set queue activity flags
    nfcServices.init();  // start NFC listner for checkin
    
}

/*
*
*
* HELPER FUNCTIONS
*
*
*/

function nowTime() { // timeNow
    var d = new Date().getTime(),    // date in milliseconds
        d2 = Math.floor(Date.now() / 1000); // date in seconds
    console.log('d is: '+d);
    console.log('d2 is: '+d2);
   // var msec = Date.parse("March 21, 2012");   //  converts data to milliseconds
  //  var d = new Date(msec);                    // converts milliseconds back to date
    
    return Math.floor(Date.now());  // date in milliseconds    //  Math.floor(Date.now() / 1000),  // date in seconds
}



// get data from API and store in array, add to list view and create markers on map, calculate         
function getFoursquareData(query){
        dataStatus  = "loading"; 
        venue       = null;
        venueInfo   = null;
        venueData   = null;
    
        $(".listItems").html("<div class='item'>Loading...</div>");
        var url = "https://api.foursquare.com/v2/venues/search?query="+query+"&ll="+myLat+","+myLng+"&intent=checkin&limit=20&v=20130203&client_id="+FOURSQUARE_CLIENT_ID+"&client_secret="+FOURSQUARE_CLIENT_SECRET+"&callback=?";
        $.getJSON(url, function(data){
            if(data.response.venues){
 
                // clear list view and map markers and arrays
                pin             =[];
                dataSt          = data.response.venues;
                markersArray    = [];  
            //    clearMarkers();
                bounds          = new google.maps.LatLngBounds();
                $(".listItems").html("<div class='query'>&darr; Showing '"+q+"'</div>");


                // add blue gps marker
                var icon = new google.maps.MarkerImage('http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',new google.maps.Size(30, 28),new google.maps.Point(0,0),new google.maps.Point(9, 28));
                var gpsMarker = new google.maps.Marker({position: new google.maps.LatLng(myLat, myLng), map: map, title: "My Position", icon:icon});
                bounds.extend(new google.maps.LatLng(myLat, myLng));
                markersArray.push(gpsMarker);

        // add all location markers to map and list view and array
                for(var i = 0; i < data.response.venues.length; i++){
                    var point = {};
                    var info;
                    point['name']       = data.response.venues[i].name;  // was  point['name']  = escaped(data.response.venues[i].name);
                    point['id']         = data.response.venues[i].id;
                    point['lat']        = data.response.venues[i].location.lat;
                    point['lng']        = data.response.venues[i].location.lng;
                    point['fAddress']   = data.response.venues[i].location.formattedAddress;
                    point['phone']      = (data.response.venues[i].contact.phone) ? data.response.venues[i].contact.phone : null;
                    point['email']      = (data.response.venues[i].contact.email) ? data.response.venues[i].contact.email : null;
                    point['twitter']    = (data.response.venues[i].contact.twitter) ? data.response.venues[i].contact.twitter : null;
                    point['referralId'] = data.response.venues[i].referralId; 
                    point['menu']       = (data.response.venues[i].menu) ? data.response.venues[i].menu : "";  /* If venue publishs a menu store it */
                    point['offerMarkerImageAddr'] = 'Logos/ct_marker_shdw_32.png';  // added this on 11/10/18
                    point['mobileUrl']  = (point['menu'].mobileUrl) ? point['menu'].mobileUrl : "";  /* If venue publishs a menu URL store it */

             // compile infor to reveal upon listview, mapview and ARview item click        
                    point['info'] = 
                        
                        "Name: "+point['name']+
                        "\\nAddress: "+point['fAddress']+
                        "\\nPhone: "+point['phone']+
                        "\\nMenu: "+point['mobileUrl']; 
                   
                    pin.push(point);
                    
                    contactInfo = pin[i].info;
                    venueInfo = pin[i];  // current instance of venue data
                    // listView list items
                   //     $(".listItems").append("<div class='item' onClick='alert(\""+contactInfo+"\");'>"+pin[i].name+"</div>"); 
                    
                   /*  $(".listItems").append("<div class='item' onClick='venueDetails(pin[\""+i+"\"]);'>"+pin[i].name+"<img data-id='img"+i+"' src='Logos/ct_marker_shdw_32.png' alt='TraQin Connect'></div>");  */
                    
                    $(".listItems").append("<div class='item showVenue' data-id='"+i+"' >"+pin[i].name+"<img class='imgScale' data-id='img"+i+"' src='" + pin[i].offerMarkerImageAddr +"' alt='TraQin AR'></div>"); 
    
                    
                    
                 //   data-href="index.php"
                  //  me.venue = pin;
                   
                    addMarker(i);
                    relativePosition(i);
                }
                map.fitBounds(bounds);
                google.maps.event.trigger(map, "resize");
                dataStatus = "loaded";
              } else if (data.meta && data.meta.errorDetail){
                    alert(data.meta.errorDetail);
                    $(".listItems").html("<div class='item'>"+data.meta.errorDetail+"</div>");
              } else {
                  $(".listItems").html("<div class='item'>Error getting data</div>");    
              }
            
        });                        
}

// add marker to map and in array        
function addMarker(i){
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(pin[i].lat, pin[i].lng), 
        map: map, 
        title: pin[i].name, 
        icon:'Logos/ct_marker_shdw_64.png'  //  'icon'
});
    bounds.extend(new google.maps.LatLng(pin[i].lat, pin[i].lng));
    markersArray.push(marker);
    
    google.maps.event.addListener(marker, 'click', function() {
    ContactDetail.init(pin[i]);
  });
    
} 

// clear all markers from map and array        
function clearMarkers() {
    'use strict';
    while (markersArray.length) {
        markersArray.pop().setMap(null);
    }
}  

// calulate distance and bearing value for each of the points wrt gps lat/lng        
function relativePosition(i){
    var pinLat = pin[i].lat;
    var pinLng = pin[i].lng;
    var dLat = (myLat-pinLat)* Math.PI / 180;
    var dLon = (myLng-pinLng)* Math.PI / 180;
    var lat1 = pinLat * Math.PI / 180;
    var lat2 = myLat * Math.PI / 180;
    var y = Math.sin(dLon) * Math.cos(lat2);
    var x = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
    bearing = Math.atan2(y, x) * 180 / Math.PI;
    bearing = bearing + 180;
    pin[i]['bearing'] = bearing;

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    distance = 3958.76  * c;
    pin[i]['distance'] = distance;
}








String.prototype.format = function () {
  var args = arguments;
  return this.replace(/\{(\d+)\}/g, function (m, n) { return args[n]; });
};















var qEventFlow = {        
                  
    qActive: null,
    sendEta: null,
    
    init: function() {
    
        this.qActive = false;  // initialize service with queue inactive
        this.sendEta = false;  // initialize service with queue inactive
    
    },
        
    offerAccept: function(offerId) {            //  qEventFlow.offerAccept();
        
        offer.accept(offerId);                  // build offer object and send to server
        
    },
    
    queuePosUpdate: function() {               //  qEventFlow.queuePosUpdate();
        
        console.log('in qEventFlow.queuePosUpdate');
        qEventFlow.queueActive.Off();  // qEventFlow.qActive = false;    
        queue.doPosUpdate();    
      //  queue.initRouteMap(myQueues[0]); // launch position every minute was setTimeout(queue.doPosUpdate(),10000);
        
    },
    
    // Wireless checkin
    requestWirelessCheckin: function() {                   //  was  qEventFlow.requestCheckin();
        navigator.notification.confirm(
            'Would you like to perform a wireless check-in?', // message
             this.onWirelessConfirm,            // callback to invoke with index of button pressed
            'Wireless Check-in',           // title
            ['No','Yes']     // buttonLabels
        );
     },
    
    onWirelessConfirm: function(buttonIndex) {
     //   me = this;
        if (buttonIndex == 2) {
            
            console.log('currentQueue.qTok is: '+ currentQueue.qTok);
            if (!qEventFlow.qActive) {
                qEventFlow.autoCheckin(currentQueue.qTok);     // send checkin token and request to server
            }

         }
        else {
            
            qEventFlow.requestNfcCheckin();
        }
    },
    
    
        // NFC
    requestNfcCheckin: function() {                  
        navigator.notification.confirm(
            'Unable to perform wireless checkin - would you like to perform a Touch-n-Go check-in?', // message
             this.onNfcConfirm,            // callback to invoke with index of button pressed
            'Touch-n-Go Check-in',           // title
            ['No','Yes']     // buttonLabels
        );
     },
    
    onNfcConfirm: function(buttonIndex) {
     //   me = this;
        if (buttonIndex == 2) {
            
            console.log('currentQueue.qTok is: '+ currentQueue.qTok);
            if (!qEventFlow.qActive) {
                nfcServices.startNdefListner();
            }
         }
        else {
            qEventFlow.requestGpsCheckin();
        }
    },

    // GPS
    requestGpsCheckin: function() {            
        navigator.notification.confirm(
            'Unable to perform Check-n-go checkin - would you like to perform a GPS check-in?', // message
             this.onGpsConfirm,            // callback to invoke with index of button pressed
            'GPS Check-in',           // title
            ['No','Yes']     // buttonLabels
        );
     },
    
    onGpsConfirm: function(buttonIndex) {
     //   me = this;
        if (buttonIndex == 2) {
            
            console.log('currentQueue.qTok is: '+ currentQueue.qTok);
            qEventFlow.autoCheckin(currentQueue.qTok);     // send checkin token and request to server

         }
        else {
            qEventFlow.declineAutoCheckin();
        }
    },
    
    declineAutoCheckin: function(){     //  qEventFlow.declineAutoCheckin();
    
        navigator.notification.alert('Welcome to Angel Cloud Resort, please checkin at the front desk');
    
    },
    
    autoCheckin: function(data) {       //  qEventFlow.//  qEventFlow.autoCheckin();();

        this.queueCheckin(data);
        
    },
    
    queueCheckin: function(data) {       //  qEventFlow.queueCheckin();
        
        checkin.do(data);
        
    },

    manualCheckinRequest: function() {   //  qEventFlow.manualCheckinRequest();
        
        navigator.notification.alert('Unable to perform mobile checkin - please visit front desk for assistance');
        
    },
    
    checkinWelcomeNotice: function() {
        
        navigator.notification.alert('Welecome to Angel Cloud '+myProfile.firstName+' '+myProfile.lastName);
        
    },
    
 /*   queueActiveOn: function() {

        this.qActive = true;
    
    },
    
    queueActiveOff: function() {
    
        this.qActive = false;
    },   */
    
    queueActive: {
    
        On: function() {

            qEventFlow.qActive = true;
        },
        
        Off: function() {

            qEventFlow.qActive = false;
        }
    },
    
/*    sendEtaOn: function() {

        this.sendEta = true;
    
    },
    
    sendEtaOff: function() {
    
        this.sendEta = false;
    },  */
    
    setSendEta: {      // qEventFlow.sendEta.Off();
    
        On: function() {

            qEventFlow.sendEta = true;
        },
        
        Off: function() {

            qEventFlow.sendEta = false;
        }
    }
    
},
    
nfcServices = {     
    
    init: function()  { 
        
        nfc.enabled(function () { // success callback
                navigator.notification.alert("NFC is available and active");
            },
            function (error) { // error callback
                navigator.notification.alert("NDEF error: " + JSON.stringify(error) +" Please activate NFC.");
            });
    
    },

    startNdefListner: function()  {

        // Read NDEF formatted NFC Tags
        nfc.addNdefListener (
            this.tagProcessor,
            function () { // success callback
                navigator.notification.alert("Please touch phone on Check-n-go point");  // waiting for tag read
            },
            function (error) { // error callback
                navigator.notification.alert("Error adding NDEF listener " + JSON.stringify(error));
            }
        );
    },
    
    tagProcessor: function(nfcEvent) {
            var tag = nfcEvent.tag,
                ndefMessage = tag.ndefMessage;

            // dump the raw json of the message
            // note: real code will need to decode
            // the payload from each record
            // navigator.notification.alert(JSON.stringify(ndefMessage));

            // assuming the first record in the message has
            // a payload that can be converted to a string.
            tagId = nfc.bytesToString(ndefMessage[0].payload).substring(3)
            if (tagId == '9561630R1915') {
                console.log("Check-n-go id is: "+tagId+" Device checked in.");
                qEventFlow.autoCheckin(currentQueue.qTok);     // send checkin token and request to server
            }
            else {
                this.stopNdefListner();
                qEventFlow.requestGpsCheckin();  // Try GPS checkin
              //  qEventFlow.manualCheckinRequest();
            }
    },
    
    stopNdefListner: function() {
        
            nfc.removeNdefListener(
                this.tagProcessor,
                function () { // success callback
                    navigator.notification.alert("Waiting for NDEF tag");
                },
                function (error) { // error callback
                    navigator.notification.alert("Error adding NDEF listener " + JSON.stringify(error));
            });          
    }
    
},


cityMapper = {

    init: function() {
        this.setup();
    },

    // setup google maps api
    setup: function() {
            $("#map").height($(window).height()-60);
            var mapOptions = {
                zoom: 13,
                mapTypeControl: false,
                streetViewControl: false,
                navigationControl: true,
                scrollwheel: true,
                navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);
        },
    
    addBlueMarker: function() {
    
            // add blue gps marker
            var icon = new google.maps.MarkerImage('http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',new google.maps.Size(30, 28),new google.maps.Point(0,0),new google.maps.Point(9, 28));
            var gpsMarker = new google.maps.Marker({position: new google.maps.LatLng(myLat, myLng), map: map, title: "My Position", icon:icon});
            bounds.extend(new google.maps.LatLng(myLat, myLng));
            markersArray.push(gpsMarker);
    
    },
    
     // add marker to local area map and in array & initiate click listener to view detail
    addMarker: function(i){ 
        var point = new google.maps.LatLng(pin[i].lat, pin[i].lng),
            markerData = {point: point, info: '', mapping: map, image: pin[i].offerMarkerImageAddr , imageScale: 50, titles: pin[i].name, infoWin: false},
            marker = cityMapper.createMarker(markerData);  // function createMarker(point, info, mapping, image, titles)   createMarker(point, info, me.map,image ,titles )

        bounds.extend(point);
        markersArray.push(marker);

        google.maps.event.addListener(marker, 'click', function() {  // Listen for local map offer marker click
            currentOffer = pin[i];
            myOffers.push(currentOffer);   // this pushes the seleted offer to the nested myOffers[] stack
            appPage.venueDetailsView(currentOffer);  // pin[i]
        });
    },        
    
    // clear all markers from local area map and array
    clearMarkers: function() {   
        'use strict';
        while (markersArray.length) {
            markersArray.pop().setMap(null);
        }
    },
    
    // Creating dynamic markers

    createMarker: function(options) {	// (point, info, mapping, image, imageScale, titles, infoWin)

      //      App.COUNT = App.COUNT + 1;
      //      navigator.notification.alert('In createMarket ' + App.COUNT);

        var icon = {
            url:        options.image, // url
            scaledSize: new google.maps.Size(options.imageScale, options.imageScale),	    // size  scaledSize: new google.maps.Size(50, 50), // scaled size   // was size
            origin:     new google.maps.Point(0,0),	            // origin
            anchor:     new google.maps.Point(32, 64)	        // anchor              anchor: new google.maps.Point(0, 0) // anchor
        },

            myMarkerOpts = {
            position:   options.point,                          // position:            point = new google.maps.LatLng(latitud,longitud)
            map:        options.mapping,
            icon:       icon,
            title:      options.titles
        },

            marker      = new google.maps.Marker(myMarkerOpts);

        if (options.infoWin) {
            var infoWinData  = {info: options.info, mapping: options.mapping, marker: marker};
            this.creatinfoWindow(infoWinData);
        } else return marker;

    },

    creatinfoWindow: function(infoWinData){      // (info, mapping, marker)

        var infoWindowOpts = {
           content: infoWinData.info

            },

            infoWindow,

            marker = infoWinData.marker;

            marker.infoWindow = new google.maps.InfoWindow(infoWindowOpts);

            google.maps.event.addListener(infoWinData.marker, 'click', function() {
              marker.infoWindow.open(infoWinData.mapping, infoWinData.marker);
           //   currentMark = marker;
            });

            google.maps.event.addListener(marker,'closeclick',function(){  //  marker may be replaced with marker.infowindow 
                marker.infoWindow.close(); 
             //   currentMark.infoWindow.close();    // close the window and remove the 
             //   currentMark.setMap(null);          //removes the marker from the map
               // then, remove the infowindows name from the array
            });


          /*      google.maps.event.addListener(map, 'click', function() {
                        infoWindow[dataId].close();
            });   */


    },
    
    // refresh gmap for venue details
    refreshMap: function() {             // not using at present
        trafficLayer.setMap(null);
        trafficLayer.setMap(gMap);
    }

},


    // toggle between list view and map view        //  depricated by DeWitt
    /* function toggleView(){
        if($(".listView").is(":visible")){
            appPage.mapView();
        } else {
            appPage.listView();
        }
    }  */


locate = {

        // calulate distance and bearing value for each of the points wrt gps lat/lng
        relativePosition: function(i, geo1, geo2, geo3, geo4){  
           // debugger;
            
            var dist = 0;
            
            if (i !== '') { 
                dist = this.getDistance(pin[i].lat, pin[i].lng, myLat, myLng);   // get distance
                pin[i]['distance'] = dist.new;    // store in venue var array
                    bearing = this.calcBearing(dist.obj.dLon, dist.obj.lat1, dist.obj.lat2);    // get bearing
                pin[i]['bearing'] = bearing;      // store in venue var array
            } 
            else if (geo1 !== '')   dist = this.getDistance(geo1, geo2, geo3, geo4);
            else                    navigator.notification.alert('No geodtic attributes provided');
            
           return dist;
            
        },      

        getDistance: function(lat1,lng1,lat2,lng2) {

                 var    R = 3958.76, // use 3959 for miles or 6371 for km
                        distObj = this.calcDistVar(lat1,lng1,lat2,lng2),  // 
                        dist =  this.calcDist(R,distObj.dLat,distObj.dLon,distObj.lat1,distObj.lat2);  
                        distance = dist;
                return { new: dist, obj: distObj }; 
            
        },

        calcDistVar: function(lat1,lng1,lat2,lng2) {

                var dLat = (lat2-lat1) * Math.PI / 180 ;
                var dLon = (lng2-lng1)* Math.PI / 180;
                lat1 = lat1 * Math.PI / 180;
                lat2 = lat2 * Math.PI / 180;

            return { dLat: dLat, dLon: dLon, lat1: lat1, lat2: lat2 };   // return variables

        },

        calcDist: function(R,dLat,dLon,lat1,lat2) {

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            distance = (R  * c).toFixed(2);  
            return distance;

        },

        calcBearing: function(dLon,lat1,lat2) {

                var y = Math.sin(dLon) * Math.cos(lat2);
                var x = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon),
                    newBearing = Math.atan2(y, x) * 180 / Math.PI;
                    newBearing = newBearing + 180;
                return newBearing;

        },
    
        // calculate direction of points and display
        calcDirection: function(degree){
            var detected = 0;
            $("#spot").html("");
            var i;
            for(i = 0;i < pin.length; i++){
                if(Math.abs(pin[i].bearing - degree) <= 20){
                    var away, fontSize, fontColor;
                    // varry font size based on distance from gps location
                    if(pin[i].distance>1500){
                        away = Math.round(pin[i].distance);
                        fontSize = "12";                           //   fontSize = "16";
                        fontColor = "#ccc";
                    } else if(pin[i].distance>500){
                        away = Math.round(pin[i].distance);
                        fontSize = "14";                           //    fontSize = "24";
                        fontColor = "#ddd";
                    } else {
                        away = pin[i].distance;          //   away = pin[i].distance.toFixed(2); 
                        fontSize = "16";                            //   fontSize = "30";
                        fontColor = "#eee";
                    }       // AR view bubble items                          // onClick='venueDetails(pin[\""+i+"\"]);'
                  //  onClick="venueDetails(pin['+i+']);"  This will replace onclick  onclick="alert(\'You are clicking on me\');"

                    var spotItem = "<div class='name showVenue' data-id='"+i+"' style='margin-left:"+(((pin[i].bearing - degree) * 5)+50)+"px;width:"+($(window).width()-100)+"px;font-size:"+fontSize+"px;color:"+fontColor+"'><img data-id='img"+i+"' src='" + pin[i].offerMarkerImageAddr +"' alt='TraQin AR' style='float:left;width:42px;height:42px'>"+pin[i].name+"<div class='distance'>"+ away +" miles away</div></div>";

                    console.log(spotItem);

                    $("#spot").append(spotItem);
 
                    detected = 1;
                } else {
                    if(!detected){
                        $("#spot").html("");
                    }
                }
            }
        },
    
        etaCalc: function(dLon1, dLat1, dLon2, dLat2) {  // incomplete and unused - useing eta from  google routes api
            var d1 = dLon1,
                d2 = dLat1,
                t1 = dLon2,
                t2 =dLat2;
            S = ((d2 - d1)/(t2 - t1));
            return S; 
        }
     
},  

ar = {
    
        // start augmented reality mode, adds camera in background
        start: function(){
         //   window.intel.xdk.display.startAR();  //  intel.xdk.display.startAR();
            $('#arView').css('background-color','transparent');
            $('body').css('background-color','transparent');
        },

        // stop intel.xdk augmented reality mode

        stop: function(){
        //    window.intel.xdk.display.stopAR();   //  intel.xdk.display.stopAR();
        },
    
        // toggle: arView on
        arView: function() {
            $("#arView").fadeIn();
            $("#topView").hide();
            document.getElementById('body').style.background = "#d22";
            ar.start();
        },

        // toggle: topView on
        topView: function() {
                $("#arView").hide();
                $("#topView").fadeIn();
                document.getElementById('body').style.background = "#fff";
                ar.stop();
        },


        stopArView: function() {
                sensor.compass.stop();
                sensor.accelerometer.stop();
                if ((watchAccelerometerID === null) || (watchCompassID === null)) {
                    $(function(){ 
                        if (($("#arView").is(":visible")))
                         this.topView();
                     //   else alert('arView is hidden');
                    });     
                }
        },

        restartArView: function() {
            if (($("#detailView").is(":hidden")) && ((watchAccelerometerID === null) || (watchCompassID === null))) {
                sensor.compass.start();
                sensor.accelerometer.start();
              //  this.arView();
            }
        }

},


data = {
    
        searchStatus: function(dataStatus) {

                $(".listItems").html("<div class='item'>"+dataStatus+"...</div>");
            
        },
    
        // new search data
        search: function(searchType){
            
            q = $("#search").val();
           // server.getData(q)
            
             switch (searchType) {
                    case 'offers':   // JSON data model version is not valid
                        myService = {
                                        'serviceType' : 'search',  
                                        'providerId'  : 'cloudtraq',
                                        'actionType'  : 'getOffers',
                                        'serviceData' : { 'query' : q } 
                        };
                        dataObj = dataObject.buildSearchObj(myService);
                        data.searchCloudTraQOffers(q)
                        break;
                // queue services    
                    case 'venues': 
                         myService = {
                                        'serviceType' : 'search',  
                                        'providerId'  : 'foursquare',
                                        'actionType'  : 'getVenues',
                                        'serviceData' : { 'query' : q } 
                        };
                        searchObj = dataObject.buildSearchObj(myService);  // searchObj.myService.serviceData.query
                        data.searchFoursquareVenues(searchObj);
                        break;
                     default :
                  // navigator.notification.alert('No service match. Server return status is: '+ searchType.status+', '+searchType.message);
                            console.log('No service match.');
             }
            
      /*      $.when(

              server.getData(q)

            ).then(function() {

             console.log("Getting Offer Data")

            });   */
            
            
        },
    
        // new search data
        searchCloudTraQOffers: function(q){
            
              var base    = dataSource.server.offertraqUrl; // "http://cmx-api-app-ruby-gtdewitt.c9users.io", "https://cloudtraq-cmx.herokuapp.com"
                    page    = "/offers/";
                //    jsonWithPadding     = "?alloworigin=false&callback=?",
                    url     = base + page;
            
            $.when(function() {

                console.log("Searching Offer Data");
                dataStatus  = "Searching offer data";
                data.searchStatus(dataStatus);
            }

            ).then(function() {

                server.getData(q)  //  This still works.  Will get all offer data from sinatra offerTraQ server

            });
            
        },
    
        // new search data
        searchFoursquareVenues: function(searchObj){
            
                    q = searchObj.myService.serviceData.query;
                    url    = "https://api.foursquare.com/v2/venues/search?query="+q+"&ll="+myLat+","+myLng+"&intent=checkin&limit=20&v=20130203&client_id="+FOURSQUARE_CLIENT_ID+"&client_secret="+FOURSQUARE_CLIENT_SECRET+"&callback=?";
 

            $.when(function() {

                console.log("Searching Venue Data");
                dataStatus  = "Searching venue data";
                data.searchStatus(dataStatus);
            }

            ).then(function() {

                getFoursquareData(q)

            });
            
        },
    
        // Store serer API offer data in array, add to list view and create markers on map, calculate
        manage: function(offerData){       // Manage server based offer data
                dataStatus  = "loading";
                data.searchStatus(dataStatus);

              //  $(".listItems").html("<div class='item'>Loading...</div>");
          
                var offerId             = "ACCR1-CM-000001-0001-000001",
                    offerMarker         = "img/ct_marker_shdw_64.png",
                    queueMarker         = "img/traqin_marker_64.png",
                    iOffers = offerData.data.offers;
                //    alert('offerData.data.offers.serviceType is: '+offerData.data.offers.serviceType );
                    //   console.log('offerData from cloudtraq is : ' + offerData);
                    if(iOffers){     //  offerData.data.offers

                        // clear list view, markers and array variables
                        offer.clearPinArray();
                        cityMapper.clearMarkers();
                        dataStatus      = "cleared";                          //  data.data.offers;
                        bounds          = new google.maps.LatLngBounds();
                        $(".listItems").html("<div class='query'>&darr; Showing '"+q+"'</div>");

                        // add blue gps user marker to local map
                        cityMapper.addBlueMarker();

                        // add all location markers to map and list view and array
                        for(var i = 0; i < iOffers.length; i++){        // offerData.data.offers.length
                            offer.loadPin(iOffers[i]);                   //   pin.push(point);

                    // listView list items

                          //     var listItem = "<div class='item' onClick='venueDetails(pin[\""+i+"\"]);'>"+pin[i].name+"<img class='imgScale' data-id='img"+i+"' src='" + pin[i].offerMarkerImageAddr +"' alt='TraQin AR'></div>"

                            var listItem = "<div class='item showVenue' data-id='"+i+"' >"+pin[i].name+"<img class='imgScale' data-id='img"+i+"' src='" + pin[i].offerMarkerImageAddr +"' alt='TraQin AR'></div>";

                            console.log(listItem);

                            $(".listItems").append(listItem);

                            cityMapper.addMarker(i);
                            locate.relativePosition(i,'','','','');
                        }
                        map.fitBounds(bounds);
                        google.maps.event.trigger(map, "resize");
                        dataStatus = "loaded";
                      } else if (data.meta && data.meta.errorDetail){
                            navigator.notification.alert(data.meta.errorDetail);
                            $(".listItems").html("<div class='item'>"+data.meta.errorDetail+"</div>");
                      } else {
                          $(".listItems").html("<div class='item'>Error getting data</div>");
                      }
            
        }
             

},


    
// start login after deviceready 
login = {
    
        // Global Variable declaration

     //   RESPONCE:   null,
     //   LOGGEDIN: false,
        
        // Login initiatlization methods
        init: function() {
            var me = this;
            this.checkPreAuth();
            this.populateSocialField();
         //   this.initValidation();

            $("#loginForm").on("submit",login.handleLogin);
            
      /*    $("#submitButton").bind("tap", function(e) {  // initialize submit register credentials button  
                me.handleLogin();
            });   */
            
        /*      $(".btnLogout").bind("tap", function() {
                logout.init();  // bind logout event to btnlogout
            });  
            
          $("#btnLogIn").bind("tap", function(e) {  // initialize login button
                me.submitLogInForm();
            });   */
            
            $("#btnRegister").bind("tap", function(e) {  // initialize submit register credentials button
              //  logout.removeLocalStorage();
                var form = $("#registerForm");

                $("#regusername", form).val("");
                $("#regpassword", form).val("");
                $("#confirmpassword", form).val("");
            /*    var form = $("#loginForm");
                $("#username", form).val("");
                $("#password", form).val(""); */
                appPage.registerView(); 
            });   

            $("#btnRegSubmit").bind("tap", function(e) {  // initialize submit register credentials button  
                logout.removeLocalStorage();
                me.handleRegister();
             //   me.submitRegisterForm();
            //    $.mobile.changePage('index.html'); 
            });  
            
            $("#btnRegCancel").bind("tap", function(e) {  // initialize submit register credentials button  btnRegCancel
            //    logout.removeLocalStorage();
            //    $("#loginform").trigger('reset');
                appPage.loginView(); 
            //    $.mobile.changePage('index.html'); 
            }); 
            
            $( "#search" ).change(function() {
              console.log( "Search box is being typed in" );
              VenueDetail.getCurrentPosition();
            });
            
           /* $('#search').keypress(function(event) {  //textarea  keyup
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });  */
            
 /* document.onkeydown = function(evt) {  // functcion writes the control character to the console
        console.log(evt.keyCode);
    }  */
            
          /*  $(document).keypress(function(e) {
              if (e.keyCode === 13) {
                    e.preventDefault();
                  e.stopPropagation();
                } 
            });  */
            
             $("#btnVenues").bind("tap", function(e) {  // initialize submit register credentials button  btnRegCancel
  
                 searchType = "venues";
                 data.search(searchType);          // get data from server

            }); 
            
            $("#btnOffers").bind("tap", function(e) {  // initialize submit register credentials button  btnRegCancel
                 searchType = "offers";
                 data.search(searchType);          // get data from server

            }); 
            
            $("#btnFinder").bind("tap", function(e) {  // initialize submit register credentials button  btnRegCancel
                 alert("Find me where I am")

            }); 

       /*     $("#btnRegCancel").bind("tap", function() { // initialize cancel register credentials button
               $.mobile.changePage('index.html'); 
            });  

             $("#btnProfile").bind("tap", function() {
               ContactForm.friendId = 0;  // Open contactform.html as account profile
               $.mobile.changePage('contactform.html'); 
            });   */
            
        },

        // pre authentication check
        checkPreAuth: function() {
            var form = $("#loginForm");
            if(window.localStorage["username"] !== undefined && window.localStorage["password"] !== undefined) {
                $("#username", form).val(window.localStorage["username"]);
                $("#password", form).val(window.localStorage["password"]);
            }
        },

        populateSocialField: function() {
           var socialNetField = $("select#openSocialId");
            for (var i = 0; i < dataSource.guestInfo.socialNets.length; i++) {
                socialNetField.append(
                    '<option value="{0}">{1}</option>'.format(
                        dataSource.guestInfo.socialNets[i].id,
                        dataSource.guestInfo.socialNets[i].name
                    )
                );
            }
         //   socialNetField.selectmenu("refresh",true);
        },
    
        populateDeviceFields: function() {

            $("#wifimacaddress").val(myMac);
            $("#deviceuuid").val(myUuid);
            $("#deviceserial").val(mySerial);
            $("#deviceplatform").val(myPlatform);
            $("#devicemanufacturer").val(myManufacturer);
            $("#devicemodel").val(myModel);
            
        },
    
        initValidation: function() {  // form validation objects
        
            var loginForm = $("form#loginForm");  // initialize validaton for login form
              loginForm.validate({
                rules: {
                    uname: {                    // login form input ID
                       required: true,
                       rangelength: [2,20]
                   },
                    pword: {                     // login form input PW
                       required: true,
                       rangelength: [6,20]
                   }
                  }
            });

            var registerForm = $("form#registerForm");  // initialize validaton for register form      
              registerForm.validate({
                rules: {
                    reguname: {                    // register form input ID
                       required: true,
                       rangelength: [2,20]
                   },
                    regpword: {                     // register form input PW
                       required: true,
                       rangelength: [6,20]
                   }
                  }
            });

       /*     var chatForm = $("form#chatform");       // initialize validaton for chat form  
              chatForm.validate({
                rules: {
                    chatmessage: {                    // register form input ID
                       required: true,
                       rangelength: [1,240]
                   }
                  }
            });  */
          
        },

        //Login services
        handleLogin: function() {
            var form = $("#loginForm");
            //disable the button so we can't resubmit while we wait
            $("#submitButton",form).attr("disabled","disabled");
            var un = $("#uname", form).val();
            var pw = $("#pword", form).val();
            var r = 0;
            var up = 0;
            console.log("click");
            var loginURI = dataSource.meshPoint.BASEURL + dataSource.meshPoint.GETLOGIN; // Server PHP MySQL login
          //  console.log('Login data is ' + loginURI);
            if(un !== '' && pw!== '') {
              //  alert('un: '+un+' pw: '+pw);
                $.post(loginURI, {u:un,p:pw,r:r,up:up}, function(data,status) {
                //    login.RESPONCE = data;
                  
                    if(status == 'success') {
                          
                        //store
                        if(data.Status == 'Valid'){
                            $("#footerNav").fadeIn();
                            // start cordova device sensors
                        /*    sensor.accelerometer.start();
                            sensor.compass.start();
                            sensor.geolocation.watchPos.start();  */

                            window.localStorage["username"] = un;
                            dataSource.guestInfo.USERNAME = un;
                            window.localStorage["password"] = pw;
                            dataSource.guestInfo.PASSWORD = pw;
                            dataSource.guestInfo.ARGS = 'u=' + un + '&p=' + pw;
                            login.GetUserAccountProfile(un,pw);
                            loggedIn = true;
                        //    dataSource.guestInfo.LOGGEDIN = true;
                        //    $("#serviceBtns").fadeIn();
                        //    $("#loginBtns").hide();
                        //    $.mobile.changePage("#loginPage");
                            appPage.listView();
                        } else { 
                            navigator.notification.alert('data.Status: ' + data.Status); 
                        }
                        
                    } else {
                        navigator.notification.alert("Your login failed");
                      //  navigator.notification.alert("Your login failed", function() {});
                    }
                    $("#submitButton").removeAttr("disabled");
                },"json"); 
            } else {
                //Thanks Igor!
                navigator.notification.alert("You must enter a username and password");
               // navigator.notification.alert("You must enter a username and password", function() {});
                $("#submitButton").removeAttr("disabled");
            }
        return false;
        },
    
            //Login services
        handleRegister: function() {
            var form = $("#registerForm");
            //disable the button so we can't resubmit while we wait
            $("#btnRegSubmit",form).attr("disabled","disabled");
            var un = $("#reguname", form).val();
            var pw = $("#regpword", form).val();
            var fn = $("#firstName", form).val();
            var ln = $("#lastName", form).val();
            var cp = $("#cellPhone", form).val();
            var em = $("#email", form).val();
            var cpw = $("#confirmpword", form).val();
            
            var dn = $("#devicemanufacturer", form).val();
            var dm = $("#devicemodel", form).val();
            var dp = $("#deviceplatform", form).val();
            var du = $("#deviceuuid", form).val();
            var dc = myCordova;
            var dv = myVersion;
            var r = 1;
            var up = 0;
            
        //    If (pw == cpw) {
            
                console.log("click");
                var loginURI = dataSource.meshPoint.BASEURL + dataSource.meshPoint.GETLOGIN; // Server PHP MySQL login
              //  console.log('Login data is ' + loginURI);
                if(un !== '' && pw!== '') {
                    $.post(loginURI, {u:un,p:pw,r:r,up:up,firstName:fn,lastName:ln,email:em,phone:cp,dn:dn,dm:dm,dc:dc,dp:dp,du:du,dv:dv}, function(data,status) {
                    //    login.RESPONCE = data;
                        if(status == 'success') {
                            //store
                            if(data.Status == 'Registered'){
                           //     window.localStorage["username"] = un;
                           //     dataSource.guestInfo.USERNAME = un;
                           //     window.localStorage["password"] = pw;
                           //     dataSource.guestInfo.PASSWORD = pw;
                           //     dataSource.guestInfo.ARGS = 'u=' + un + '&p=' + pw;
                           //     login.GetUserAccountProfile(un,pw);
                           //     dataSource.guestInfo.LOGGEDIN = true;
                                
                            //    $("#serviceBtns").fadeIn();
                            //    $("#loginBtns").hide();
                            //    $.mobile.changePage("#loginPage");
                                appPage.loginView();
                            }  else { 
                                navigator.notification.alert('data.Status: ' + data.Status); 
                            }
                            
                        } else {
                            navigator.notification.alert("Your login failed");
                          //  navigator.notification.alert("Your login failed", function() {});
                        }
                        $("#btnRegSubmit").removeAttr("disabled");
                    },"json"); 
                } else {
                    //Thanks Igor!
                    navigator.notification.alert("You must enter a username and password");
                   // navigator.notification.alert("You must enter a username and password", function() {});
                    $("#btnRegSubmit").removeAttr("disabled");
                }
                return false;
        //    } else navigator.notification.alert('Password confirmation does not match');
 
        },
    
        
        GetUserAccountProfile: function(username,password) {
            var getUserUrl = dataSource.meshPoint.BASEURL + dataSource.meshPoint.GETUSERDATA;   // Get user profile from server
            var args = 'u=' + username + '&p=' + password + '&ap=1';
               $.post(getUserUrl, args , function(data, status){

                if (status == 'success') {
                    if (data.length > 0) {
                        var obj = JSON.parse(data);
                        var userProfile = obj.user;    
                        var firstName = obj.user[0].firstName;   
                       console.log("User profile data is: " + data);
                        console.log("firstName is: " + firstName);
                        PropertyViewObjectsDB.importTable('profile',userProfile);
                        dataSource.meshPoint.USERPROFILE = userProfile;
                        console.log('dataSource.meshPoint.USERPROFILE[0].firstName is: '+ dataSource.meshPoint.USERPROFILE[0].firstName);
                        userProfile = null;
                        // get device & user info and store in global
                        user.getPersonalInfo();
                    } else {
                       navigator.notification.alert("No profile");
                    } 
                } else {
                     navigator.notification.alert("Status: " + status);
                }  
               });  
        },    
    
  /*      submitRegisterForm: function() {
            var me = this;
            var form = $("form#registerForm");
            var validator = form.validate();
            validator.form();
              if (validator.numberOfInvalids() == 0) {
                  $('#registerdialog').dialog('close')  // close the register dialog box
                  var fields = form.serializeArray();

                console.log("fields reg: " + fields[4].name + " " + fields[4].value);  // username
                  console.log("fields reg: " + fields[5].name + " " + fields[5].value);  // password
                  myUsername = fields[4].value;  // was App.USERNAME
                  myPassword = fields[5].value;  // was App.PASSWORD
                  dataSource.meshPoint.REGISTER = 1;              // ws App.REGISTER      
                //  me.ServerLogIn(fields);            
                  me.ServerLogIn();     
              } else {
                  navigator.notification.alert("Invalid input.  Please correct your data input.");
              }
               // App.REGISTER = 0;
          },
    
        ServerLogIn:  function() {  // if App.REGISTER = 0 login, if 1 register new user - was function(credentials) 

            var me = this;
        
       //     App.USERNAME = credentials[0].value;
       //     App.PASSWORD = credentials[1].value;
            var loginURI = dataSource.meshPoint.BASEURL + dataSource.meshPoint.GETLOGIN  // Server PHP MySQL login
            var args = 'u=' + myUsername + '&p=' + myPassword + '&r=' + dataSource.meshPoint.REGISTER +  '&up=' + dataSource.meshPoint.UPDATEPROFILE; 
            if(dataSource.meshPoint.REGISTER ==1)  args += '&dn=' + myManufacturer + '&dm=' + myModel  + '&dc=' + myCordova + '&dp=' + myPlatform + '&du=' + myUuid + '&dv=' + myVersion; 
            if(dataSource.meshPoint.UPDATEPROFILE ==1)  args += "&" + dataSource.meshPoint.PROFILEMODS; // up = update profile yes or no, pm = profile modifications - new data
            $.post(loginURI, args , me.LoginStatus);
        },
    
    LoginStatus: function(data, status) {
       // navigator.notification.alert("In LoginStatus, this is " + data);
 
               if (data == 'Error')
               {
                  navigator.notification.alert('There was an error - please try again.')
               }
               else if (data == 'Invalid')
               {
                  navigator.notification.alert('That Username is taken or the Password is incorrect' +
                     ' - please try again')
               }
               else if (data == 'Registered')
               {
                   if (dataSource.meshPoint.REGISTER == 1) {
                       navigator.notification.alert(myUsername + 'User has been registered'); 
                       dataSource.meshPoint.REGISTER = 0;
                       appPage.loginView();
                   } else if (dataSource.meshPoint.UPDATEPROFILE = 1) {
                       navigator.notification.alert(myUsername + 'Profile has been updated'); 
                       dataSource.meshPoint.UPDATEPROFILE = 0;
                       dataSource.meshPoint.PROFILEMODS = null;
                   }
                   
               }
               else {
                    if(dataSource.meshPoint.REGISTER == 0) {   // valid credentials - login to system
                       // systemLogin.SetLogin();   
                                                
                        appPage.loginView();

                          if (dataSource.meshPoint.LOCALSTORAGE)
                          {
                             localStorage.setItem('username', myUsername)
                             localStorage.setItem('password', myPassword)
                          }
                          else
                          {
                             ProcessCookie('save', 'username', myUsername, 31556926)
                             ProcessCookie('save', 'password', myPassword, 31556926)
                          }



                   } else {
                       navigator.notification.alert("Registration Error - please reenter username and password");
                   }    

               }
      },    */
    
    
    showForm: function() {

               /*         <fieldset class="ui-responsive" id="serviceBtns">
                        <!-- input field to enter key -->
                        <label  for="devkey">Please enter your Movie DB developer key to begin:</label>  <!-- ft972rdet2fdkcukhugr6b8p -->
                        <input  type="text" value="fd400573a7b10703351406a93456d62c"  placeholder="Enter Key . . ." name="devkey" id="devkey"/>
                     <!-- button to display instructions dialog #getKeyMsg -->
                        <div class="ui-grid-a ui-responsive">
                          <!--  <div class="ui-block-a"><a href="#getKeyMsg" data-role="button">Get Key</a></div> -->

                            <div class="ui-hide-label ui-block-a" data-uib="jquery_mobile/select">  <!-- with-label  -->
                              <label for="beerId" class="narrow-control">Select Beer</label>
                           <div data-role="fieldgroup" class+="" "wide-control"="">
                                <select data-iconpos="right" name="beerId" id="beerId">
                                  <option>Select a Service Key</option>
                                </select>
                             </div>  
                            </div>  


                            <div class="ui-block-b"><button type="submit" data-theme="b">Do Submit</button></div>
                        </div>
                    </fieldset>  */

       var formHtml = '<fieldset class="ui-responsive" id="serviceBtns">' +
           ' <div class="upage-content contactBox"> ' +
              ' <form id="contactform"> ' +
                       ' <div "> ' +
                        '  <label class="narrow-control"></label> ' +
                         ' <input class="wide-control" placeholder="First Name" type="text" name="firstName" id="firstName" style="padding:10px"> ' +
                       ' </div> ' +
                        ' <div> ' +
                         ' <label class="narrow-control" for="lastName"></label> ' +
                          ' <input class="wide-control" placeholder="Last Name" type="text" name="lastName" id="lastName" style="padding:10px"> ' +
                       ' </div> ' +
                        ' <div > ' +
                         ' <label class="narrow-control" for="address"></label> ' +
                         ' <input class="wide-control" placeholder="Address" type="text" name="address" id="address" style="padding:10px"> ' +
                        ' </div> ' +
                        ' <div > ' +
                         ' <label class="narrow-control" for="zipcode"></label> ' +
                         ' <input class="wide-control" placeholder="Zip Code" type="number" name="zipcode" id="zipcode" style="padding:10px"> ' +
                       ' </div> ' +
                        ' <div > ' +
                         ' <label class="narrow-control" for="email"></label> ' +
                         ' <input class="wide-control" placeholder="you@you.com" type="text" name="email" id="email" style="padding:10px"> ' +
                       ' </div> ' +
                        ' <div > ' +
                         ' <label class="narrow-control" for="phone"></label> ' +
                         ' <input class="wide-control" placeholder="Tel" type="tel" name="phone" id="phone" style="padding:10px"> ' +
                       ' </div> ' +
                        ' <div > ' +
                         ' <label class="narrow-control"></label> ' +
                         ' <div data-role="fieldgroup" class+="" "wide-control"=""> ' +
                           ' <select data-iconpos="right" name="beerId" id="beerId"> ' +
                             ' <option>Select a Beer</option> ' +
                           ' </select> ' +
                         ' </div> ' +
                       ' </div> ' +
                       ' <div > ' +
                         ' <label class="narrow-control label-inline" for="lat">Lat</label> ' +
                         ' <input class="wide-control" placeholder="Latitude" type="text" name="lat" id="lat" disabled style="padding:10px"> ' +
                       ' </div> ' +
                       ' <div > ' +
                         ' <label class="narrow-control label-inline" for="lng">Lng</label> ' +
                         ' <input class="wide-control" placeholder="Longitude" type="text" name="lng" id="lng" disabled style="padding:10px"> ' +
                       ' </div> ' +   
               ' </form> ' +
            ' </div> ' +
            '</fieldset>';

        $("#regform").html(formHtml);
    }

},
    
    
/*
 *   LOGOUT CONTROLLER ********************************************************************************************
 */         
   
logout = {
    
    init: function() {
        navigator.notification.confirm(
            'would you like to Logout?', // message
             this.onConfirm,            // callback to invoke with index of button pressed
            'Exit TraQin',           // title
            ['No','Yes']     // buttonLabels
        );
     },
    
    onConfirm: function(buttonIndex) {
     //   me = this;
        if (buttonIndex == 2) {
         //   logout.LoggingOut();
            if (qEventFlow.qActive)  queue.cleanup(); // turn off eta to server turn off sendEta qEventFlow.qActive
            logout.removeLocalStorage();
            appPage.loginView();

         }
        else {
            navigator.notification.alert('Great decision - stay with us a while');
        }
    },
            
    LoggingOut: function() {
         //  $(".btnLogout").hide();
            $("#btnMapIt").hide();
            $("#btnProfile").hide();
            $("#footerNav").hide();
            $("#headerTitle > h1").html("zoneTraQer");
            $("#loginform").trigger('reset'); 
            MapMonitor.DisplayMainImg();  
            $("#mainpageimg").fadeIn();
            $("#mediatext").fadeIn();
            $("#loginform").fadeIn();  //loginform
            $("#btnRegister").fadeIn();
            $("#socialLogin").fadeIn();   
            clearInterval(App.CHATTOUT);  
            clearInterval(App.LOCTOUT);
            clearTimeout(App.IDLETIMER); 
        
                //  $("#chatbox").hide();
        
       //     S('mapit').opacity  = '0' 
        
// this was active -             intel.xdk.notification.deletePushUser();
        //    AppMobi.notification.deletePushUser();
             //   clearInterval(App.CHATTOUT);
            //  clearTimeout (App.CHATTOUT);


          //  clearTimeout (TOUT);
    
        },
        
        removeLocalStorage: function () {

        /*    var form = $("#loginForm");

                $("#uname", form).val("");
                $("#pword", form).val("");  */
            $("#loginForm").trigger('reset');

           if(window.localStorage["username"] !== undefined && window.localStorage["password"] !== undefined)
           {
              localStorage.removeItem('username');
              localStorage.removeItem('password');
           }
         /*  else
           {
              ProcessCookie('erase', 'username');
              ProcessCookie('erase', 'password');
           }  */
            PropertyViewObjectsDB.runQuery('DROP TABLE profile', function(){   // CloudtraqObjectsDB
                navigator.notification.alert("profile table dropped");  
            });   
            
            loggedIn = false;
            // clear map in canvas
       /*     $("#map").empty();
            $("#map2").empty();   */
            // stop cordova device sensors
      /*      sensor.accelerometer.stop();
            sensor.compass.stop();
            sensor.geolocation.watchPos.stop();  */

        }
        
},


    
// helper functions
helper = {
    
    escaped: function(str) {
       // var str = document.getElementById("demo").innerHTML;
        var res = str.replace(/'/g, "");        //  var res = str.replace(/blue/gi, "red");   Global case insensitive
       // document.getElementById("demo").innerHTML = res;
        return res;
    }

},


sensor = {
    
    geolocation: {
        
        watchPos: {

            // Start watching the geolocation
            start: function(){
                var options = { timeout: geoTimer };
                watchGeoID = navigator.geolocation.watchPosition(sensor.geolocation.watchPos.onGeoSuccess, sensor.geolocation.watchPos.onGeoError, options);
                geoTimer = 30000;
            },

            // Stop watching the geolocation
            stop: function() {
                if (watchGeoID) {
                    navigator.geolocation.clearWatch(watchGeoID);
                    watchGeoID = null;
                }
            },

            // onSuccess: Get the current location
            onGeoSuccess: function(position) {
                'use strict';
                document.getElementById('geolocation').innerHTML = 'Latitude: ' + position.coords.latitude + '<br />' + 'Longitude: ' + position.coords.longitude;
                myLat       = position.coords.latitude;    // Every 30 sec myLat & myLng updated with current position
                myLng       = position.coords.longitude;
                console.log("I'm in onGeoSuccess");
                console.log('qEventFlow.qActive in onGeoSuccess is: '+qEventFlow.qActive);
                if (qEventFlow.qActive) {               
                    setTimeout(qEventFlow.queuePosUpdate(),10000);
                }
            },

            // onError: Failed to get the location
            onGeoError: function(err) {
                document.getElementById('log').innerHTML += "onError=.";
                console.warn('ERROR(' + err.code + '): ' + err.message);
            }  
            
        }

    },

    
    compass: {    

        // Start watching the compass
        start: function() {
            var options = { frequency: 500 };
            watchCompassID = navigator.compass.watchHeading(sensor.compass.onCompassSuccess, sensor.compass.onCompassError, options);
        },

        // Stop watching the compass
        stop: function() {
            if (watchCompassID) {
                navigator.compass.clearWatch(watchCompassID);
                watchCompassID = null;
            }
        },

        // onSuccess: Get the current heading
        onCompassSuccess: function(heading) {
            var directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N'];
            var direction = directions[Math.abs(parseInt((heading.magneticHeading) / 45) + 0)];
            document.getElementById('compass').innerHTML = heading.magneticHeading + "<br>" + direction;
            document.getElementById('direction').innerHTML = direction;
            var degree = heading.magneticHeading;
            if($("#arView").is(":visible") && dataStatus != "loading"){
                locate.calcDirection(degree);
            }
        },

        // onError: Failed to get the heading
        onCompassError: function(compassError) {
            document.getElementById('log').innerHTML += "onError=."+compassError.code;
        }

    },
    
    accelerometer: {

        // Start checking the accelerometer
        start: function() {
            var options = { frequency: 100 };
            watchAccelerometerID = navigator.accelerometer.watchAcceleration(sensor.accelerometer.onSuccess, sensor.accelerometer.onError, options);
        },

        // Stop checking the accelerometer
        stop: function() {
            if (watchAccelerometerID) {
                navigator.accelerometer.clearWatch(watchAccelerometerID);
                watchAccelerometerID = null;
            }
        },

        // onSuccess: Get current accelerometer values
        onSuccess: function(acceleration) {
            // for debug purpose to print out accelerometer values
            var element = document.getElementById('accelerometer');
            element.innerHTML = 'Acceleration X: ' + acceleration.x + '<br />' +
                                'Acceleration Y: ' + acceleration.y + '<br />' +
                                'Acceleration Z: ' + acceleration.z ;
            if((acceleration.y > 7) && (!offerView) && (loggedIn)){
                ar.arView();    //  Turn on arView
            } else {
                ar.topView();   //  Turn on topView
            }
        },

        // onError: Failed to get the acceleration
        onError: function() {
            document.getElementById('log').innerHTML += "onError.";
        }
    
    }
    
},

// Update app pages - fadeIn page & update correct navbar

appPage = {
    
    init: function() {                    // turn off all views
            $(".mapView").hide();
            $(".detailView").hide();
            $(".inviteView").hide();
            $(".formView").hide();
            $(".listView").hide();
            $(".loginView").hide();  
            $(".registerView").hide();
        
    },
    
    setNavBar: function() {

      //  console.log("device.uuid is :  " + myUuid);    &hellip;  ... symbole
        var navbarHtml =   ' <div class="navbtn btnRt" id="btnRtOld" onclick="appPage.refresh();"> ' +
                           ' Refresh ' + 
                         ' </div> ' +   
                /*        ' <div class="navbtn btnRt" id="btnRtOld" onclick="logout.init();"> ' +
                           ' Logout ' +
                         ' </div> ' +   */
                         ' <div class="navbtn btnLft" id="btnLftOld" onclick="appPage.mapView();"> ' +
                           ' Map ' +
                         ' </div> ' +
                         ' <div class="navtitle" id="navTitle"> TraQin Venue Search' +
                         ' </div> ' ;
        $(".navbar").html(navbarHtml);
    },
    
    setFooterNavBar: function() {   
    
        if($(".listView").is(":visible")){ 
            $("#footerNav").fadeIn();
        } else {
            $("#footerNav").hide();
        }
    
    },
    
    loginView: function() {
        
        offerView = false;      // turn on arView
        qEventFlow.setSendEta.Off();  //sendEta = false;        // turn off eta to server
        qEventFlow.queueActive.Off();       // qEventFlow.qActive = false;        // turn off queue activities
        currentOffer = {};        // empty current offer selected variable
        currentQueue = {};        // empty current offer selected variable
        this.init();
        $(".loginView").fadeIn();
        this.setNavBar();         // Update navbar with contact details message and buttons 
        $(".navbar").html( ' <div class="navtitle" id="navTitle"> TraQin Venue Search </div> ' );
        this.setFooterNavBar();
    },
    
    registerView: function() {
        
        this.init();
        $(".registerView").fadeIn( function(){ $("#registerForm").trigger('reset');  login.populateDeviceFields(); });
        this.setNavBar();         // Update navbar with contact details message and buttons 
        $(".navbar").html( ' <div class="navtitle" id="navTitle"> TraQin Registration </div> ' );
        this.setFooterNavBar();
    },

    listView: function() {
        
        offerView = false;      // turn on arView        
        this.init();
        $(".listView").fadeIn();
        this.setNavBar();         // Update navbar with contact details message and buttons 
        $('div.btnRt').html('Logout');
        $('.btnRt').attr('onclick', 'logout.init()');
        if (qEventFlow.qActive) {   //  !$.isEmptyObject(currentQueue)
            queue.cleanup(); // turn off eta to server turn off sendEta qEventFlow.qActive
        }
        this.setFooterNavBar();
    },

    mapView: function() {
        
        if(!dataStatus){
            navigator.notification.alert("No search data. Perform search before selecting map.");
        } else {
            this.init();

         //   myLat = isNaN(myLat) || myLat === null ?  37.4419 : myLat;  // bound error if no search prior to map
         //   myLng = isNaN(myLng) || myLng === null ? -122.1419 : myLng;

            $("#map").height($(window).height()-60);
            $(".mapView").fadeIn(function(){google.maps.event.trigger(map, "resize");map.fitBounds(bounds);});

            this.setNavBar();    // Update navbar with contact details message and buttons 
                $('div.navtitle').html('TraQin Venue Map');
                $('div.btnLft').html('List');
                $('.btnLft').attr('onclick', 'appPage.listView()');
                $('.btnRt').attr('onclick', 'appPage.refresh()');
        }  
        this.setFooterNavBar();
    },

    venueDetailsView: function(currentOfferData) {
       
        offerView = true;  // turn off arView
        this.init();
        $("#detail").height($(window).height()-60);
        $(".detailView").fadeIn(function(){VenueDetail.showDetails(); VenueDetail.init(currentOfferData);});  //  VenueDetail.init(currentOfferData);
        
        this.setNavBar();   // Update navbar with contact details message and buttons 
            $('div.navtitle').html('TraQin Venue Details');
            $('div.btnLft').html('List');
            $('div.btnRt').html('Edit');   // launch guest invite page
            $('.btnLft').attr('onclick', 'appPage.clearListview()');
            $('.btnRt').attr('onclick', 'appPage.formView()');    //  $('.btnRt').attr('onclick', 'appPage.formView()'); 
            this.setFooterNavBar();
    },
    
    queueView: function() {
       
        offerView = true;  // turn off arView
     //   this.init();
     //   $("#detail").height($(window).height()-60);
     //   $(".detailView").fadeIn(function(){VenueDetail.showDetails(); VenueDetail.init(currentOfferData);});  //  VenueDetail.init(currentOfferData);
     //   $(".inviteView").hide();
     //   $(".detailView").fadeIn();
        $('div.navtitle').html('TraQin Queue Session');
     //   $('div.btnLft').html('Cancel');
        $('div.btnRt').html('Invite');   // launch guest invite page
     //   $('.btnLft').attr('onclick', 'history.back()');  // <button type="button" onclick="history.back();">Back</button>

        $('.btnRt').attr('onclick', 'appPage.inviteView()');    //  $('.btnRt').attr('onclick', 'appPage.formView()');
        this.setFooterNavBar();
    },
    
    connectView: function() {
        
        offerView = true;  // turn off arView
 
        $('div.navtitle').html('TraQin Venue Connect');
        this.setFooterNavBar();
    },
    
    
    inviteView: function() {  // needs work  goes back to logout status
        
      //  this.init();
      //  $("#regform").height($(window).height()-60);
        $(".detailView").hide();
        $(".inviteView").fadeIn(function(){login.showForm();});

        this.setNavBar();   // Update navbar with contact details message and buttons 
            $('div.navtitle').html('TraQin Queue Invite');
            $('div.btnLft').html('Back');
            $('div.btnRt').html('refresh');
         //   $('.btnLft').attr('onclick', 'appPage.venueDetailsView()');
        
            $('.btnLft').attr('onclick', function(){ $(".inviteView").hide(); $(".detailView").fadeIn(); queue.queuePosUpdate()});  // <button type="button" onclick="history.back();">Back</button>
            $('.btnRt').attr('onclick', 'appPage.refresh()');  

        this.setFooterNavBar();
        VenueDetail.getFormData();  // Data fill the contact details page
    },
    
    

    formView: function() {  // needs work  goes back to logout status
        
        this.init();

        $("#regform").height($(window).height()-60);
        $(".formView").fadeIn(function(){login.showForm();});

        this.setNavBar();   // Update navbar with contact details message and buttons 
            $('div.navtitle').html('TraQin Venue Edit');
            $('div.btnLft').html('Back');
            $('div.btnRt').html('refresh');
         //   $('.btnLft').attr('onclick', 'appPage.venueDetailsView()');
        
            $('.btnLft').attr('onclick', 'history.back()');  // <button type="button" onclick="history.back();">Back</button>
            $('.btnLft').attr('class', 'navbtn btnLft back');
            $('.btnRt').attr('onclick', 'appPage.refresh()');  
        
            $( document ).ready(function() {
                $('.back').click(function(){
                  //  parent.history.back();
                    history.go(-1);
                    return false;
                });
            });

        this.setFooterNavBar();
        VenueDetail.getFormData();  // Data fill the contact details page
    },
    
    // refresh new data
    refresh: function(){
            appPage.listView();
            data.search();
    },
    
    // clear list view
    clearListview: function(){
            
            $('.listItems').empty();
            //  $(".listItems").html("");
            $("#search").val('');
            appPage.listView();
            currentoffer = {};
            currentQueue = {};       
            while (myOffer.length) {
                myOffer.pop().setMap(null);
            }
            while (myQueue.length) {
                myQueue.pop().setMap(null);
            }
            dataStatus = 0;
    }
},
    

dataSource = {  
    
    /* zoneTraQer Server API data section ----------------------------------------------------------------- */
    
        /* URL to access meshPoint server API   */
    
    server: {
        
        offertraqUrl: "http://cmx-api-app-ruby-gtdewitt.c9users.io"  //  "https://cloudtraq-cmx.herokuapp.com"
        
    },
    
    meshPoint: {   
        
                        ARGS: null,  /* AJAX parameter argument variable
                        
                        /* base url for all zoneTraQer API */
                        BASEURL: 'http://68.96.166.93:888/',    

                        /* page url for user prifile API */
                        GETUSERDATA: 'getuserdata2.php',   // get user profile information  - 'traqingetusers.php' - getuserdata.php  
        
                        /* page url for device frame data API */
                        GETLOCARRAY: 'getlocjsonarrays.php',   // get device frame information   
        
                        LOGIN: 'getlogin2.php',  //     'getlogin.php'  'traqinlogin.php'
        
                        /* page url for user account API */
                        GETLOGIN:  'getloginjson.php',            //   'getlogin2.php'  'traqinlogin.php'
                
                        REGISTER: 0,
        
                        UPDATEPROFILE: 0, // set when updating profile on server
        
                        USERPROFILE: null,    // variable to cache user's profile
        
                        PROFILEMODS: null, // holds updated profile data to send to server
        
                        LOCALSTORAGE: true, // store user name password locally
        
                        /* Physical venue nodes and servers - global Variables */
                     //	MCHQ: [], /* Microclient cloudTraQ Network*/    
                        CTSTYPE: 0,  // set cloudtraq service type variable
                        CTSNAME: null,   // set cloudtraq service name variable
                        FLOORPLAN: null,
                        RESOURCES: null,
                        ELEMENTS: null,
                        BLUEDOTS: null,
                        HUBS: [],
                        NODES: [],

        
    },
    
    /* End zoneTraQer server section ------------------------------------------------------------------------------ */
    
    
    
    /* developer key to access zoneTraQer guest & staff context web APIs    */
    
    guestInfo: {
                        key: '',
        
                        USERNAME: null,
        
                        PASSWORD: null, 
        
                   //     ARGS: null,
        
                        FIRSTLOC: true, // To initialize getloc function

        
                //zoneTraQer login & register - Global variables
                        LOGGEDIN: false,
                        WLOGGEDIN: false,
                 //       REGISTER: 0, // set until determine registered
                 //       UPDATEPROFILE: 0, // set when updating profile on server
                        PROFILEMODS: null, // holds updated profile data to send to server
                      //  PROFILE: [],
                        PRIV: 1,
                        socialNets: 
                            [
                             {"id":0, "name":"facebook"},
                             {"id":1, "name":"foursquare"}, 
                             {"id":2, "name":"instagram"}, 
                             {"id":3, "name":"google"}, 
                             {"id":4, "name":"linkedin"}
                            ] // single sign on option

    }
    
/* End zoneTraQer guest profile section ----------------------------------------------------------------- */
    
    
},
    

user = {               //  myProfObj = { 'myProfile': null, 'myDevice': null, 'myOffers': [], 'myQueues': [] } 
    
    getPersonalInfo: function() {

        this.myProfile.getInfo();

    },    

    
    getDeviceInfo: function() {
        
        (function() {       // this plugin only runs on live device
            window.MacAddress.getMacAddress(
                function(macAddress) {user.getMyMacAddr(macAddress);}, function(fail) {alert(fail);}  
            );
        })();
    
        this.myDevice.getInfo();
     //   if (myMac === '' || myMac === null || myMac === undefined) myMac = "B0:72:BF:E2:4B:AE";  // thisMac = "mac_unavailable";

    },
    
    // get my mac address from 
    getMyMacAddr: function(gotMac){  // this method only runs on live device from Plugin method in getPersonalInfo()
        var theMac = '';
        (function() {  
            var thisMac = "";  // my samsung B0:72:BF:E2:4B:AE
            if (gotMac === '' || gotMac === null || gotMac === undefined) thisMac = "mac_unavailable";  // thisMac =   "B0:72:BF:E2:4B:AE";
            else thisMac = gotMac;
            theMac = thisMac;
        })();
        myMac = theMac;
    },
    
    myDevice: {
        
        // get my mac address from 
        getInfo: function(){

                myUuid          = device.uuid;
                mySerial        = device.serial;
                myModel         = device.model;
                myPlatform      = device.platform;
                myVersion       = device.version;
                myManufacturer  = device.manufacturer;
                myVirtual       = device.virtual;
                myCordova       = device.cordova;

            //    setTimeout(user.device(),500);               // build device object of myProfObj
        }
        
    },  
    
    myProfile: {
        
        getInfo: function(){

            // get user from database

                myUsername      = dataSource.meshPoint.USERPROFILE[0].userName; // 'gdewitt';  
                myFirstName     = dataSource.meshPoint.USERPROFILE[0].firstName; // 'Greg';
                myLastName      = dataSource.meshPoint.USERPROFILE[0].lastName; // 'DeWitt';
                myCell          = dataSource.meshPoint.USERPROFILE[0].phone; // '7027673908';
                myEmail         = dataSource.meshPoint.USERPROFILE[0].email; // 'gdewitt@cloudtraq.com';

        }
          

    }  
    

},
    
    
dataObject = {  

    
    myProfile: function() {
        
            var myProfileObj = {};

            (function(){

                var myObj   = {
                        'username'  : myUsername,
                        'firstName' : myFirstName,
                        'lastName'  : myLastName,
                        'cell'      : myCell,
                        'email'     : myEmail
                };
                myProfileObj = myObj;

             })();

            myProfile = myProfileObj;   // example: myProfile.username
        
                    console.log('myProfile.username is : ' + myProfileObj.username);              
                    console.log('myProfile.firstName is : ' + myProfileObj.firstName);      
                    console.log('myProfile.lastName is : ' + myProfileObj.lastName);
                    console.log('myProfile.cell is : ' + myProfileObj.cell);
                    console.log('myProfile.email is : ' + myProfileObj.email);

            return myProfileObj;
               
    },
    
    myDevice: function(){  

                var myDeviceObj = {};
                if (myMac === '' || myMac === null || myMac === undefined) myMac = "mac_unavailable";  // b0:72:bf:e2:4b:ae  mac_unavailable

                (function(){

                    var myObj   = {

                        'mac'           : myMac,            
                        'uuid'          : myUuid,
                        'serial'        : mySerial,
                        'model'         : myModel,
                        'platform'      : myPlatform,
                        'version'       : myVersion,
                        'manufacturer'  : myManufacturer,
                        'virtual'       : myVirtual,
                        'cordova'       : myCordova

                    };
                    myDeviceObj = myObj;


                 })();

                myDevice = myDeviceObj;  // example: myDevice.mac
        
        
                console.log('myDevice.mac is : ' + myDeviceObj.mac);              
                console.log('myDevice.uuid is : ' + myDeviceObj.uuid);      
                console.log('myDevice.serial is : ' + myDeviceObj.serial);
                console.log('myDevice.model is : ' + myDeviceObj.model);
                console.log('myDevice.platform is : ' + myDeviceObj.platform);
                console.log('myDevice.version is : ' + myDeviceObj.version);           
                console.log('myDevice.manufacturer is : ' + myDeviceObj.manufacturer);
                console.log('myDevice.virtual is : ' + myDeviceObj.virtual);
                console.log('myDevice.cordova is : ' + myDeviceObj.cordova);
        

                return myDeviceObj;    
        
    },
    
    myOffer: function(offerId){
        
        // Accept from server data.offers[0] currentOffer = {"data":{"offers":[{"id":1,"brand":"Angel Cloud Resort","brandId":"ACCR1","brandLocation":"Henderson","brandAddress":"2675 Windmill Pkwy Suite   #2222","brandCity":"Henderson","brandState":"Nevada","brandZipcode":"89074","floor":"Valet","brandTel":"7027673908","brandEmail":"info@angelcloudresort.com","brandUrl":"http://www.angelcloudresort.com/","brandTwitter":"@angelcloudresort","brandFacebook":"https://www.facebook.com/angelcloudresort","brandInstagram":"https://www.instagram.com/angelcloudresort/","brandLinkedin":"https://www.linkedin.com/company/angelcloudresort","nodeId":"CM-000001-0001-000001","nodeType":"WIFI/BT/BLE","lat":36.040286,"lng":-115.103222,"prodOrServ":"product","prodOrServName":"Coffee","prodOrServNum":1001,"serviceType":"room","offerId":"ACCR1-CM-000001-0001-000001","offerType":"Special Deal","offerMarkerImageAddr":"img/traqin_hot_deal_3_64.png","queueMarkerImageAddr":"img/traqin_marker_64.png","offerName":"Coffee time","description":"VIP sip for free","startDate":"2017-07-05","stopDate":"2017-07-12","created_at":"2017-07-26T03:25:53+00:00"}]}}
        
        // load currentOffer variable with this myOffer data, then creaate the offerAcceptObj below to send to server.
        
            var offerAcceptObj =  { offerId: offerId,                        // Only attribute returned from offer       
                                    mac: myDevice.mac,                                    // samsung mac B0:72:BF:E2:4B:AE  myDevice.mac
                                    uuid: myDevice.uuid,                                  //  myUuid
                                    serial: myDevice.serial,                               //mySerial, 
                                    platform: myDevice.platform,                           // myPlatform,
                                    model: myDevice.model,                                     // myModel,
                                    version: myDevice.version,                                // myVersion
                                    manufacturer: myDevice.manufacturer,                      // myManufacturer
                                  //  virtual: myVirtual,
                                    cordova: myDevice.cordova,                                 // myCordova,
                                    location: { 
                                        lat: myLat,                                          // myUuid
                                        lng: myLng,                                         // myUuid
                                        distance: dataObject.myDist(myLat,myLng)
                                    },
                                    user: {
                                        username:   myProfile.username, // myProfile.username
                                        firstName:  myProfile.firstName,
                                        lastName:   myProfile.lastName,
                                        email:      myProfile.email,
                                        cell:       myProfile.cell
                                    },
                                    acceptToken: null,
                                    queueMarkerImageAddr:   currentOffer.queueMarkerImageAddr, 
                                    offerMarkerImageAddr:   currentOffer.offerMarkerImageAddr,
                                    myService: {
                                                    serviceType:  currentOffer.serviceType,        // Type of service provided by retailer
                                                    providerId:   currentOffer.providerId,         // Equipement vendor ID providing service
                                                    actionType:   currentOffer.actionType         // Type of action being provided with this request
                                    },
                                    timeStamp: nowTime()                                    // date in milliseconds
                            };
     //   alert('offerAcceptObj.location.distance is '+ offerAcceptObj.location.distance);
     //   alert('dataObject.myDist is '+ dataObject.myDist(myLat,myLng));
     //      alert('offerAcceptObj.serviceType is '+ offerAcceptObj.serviceType);
        
        return offerAcceptObj;
        
    },
    
    buildMyQueueObj: function(queueData) {
        
        var dataObj = {};
        
        // Return data queueData = {"status":"queue saved","username":"gdewitt","offerName":"Coffee time","offerMarkerImageAddr":"img/traqin_hot_deal_3_64.png","queueMarkerImageAddr":"img/traqin_marker_64.png","resTime":"2017-08-16 22:23:04 +0000","queueTokenHash":"vQZWUeXRu9JKIgEoDezFqOV7XY2tQNHKYe2LRBZrwtkwAAHPpJNge","location":{"lat":36.0370545239789,"lng":-115.10198898647694},"acceptToken":"X.Hv0euBR1tR3R0pRreaMOwmcKVSh6EeOsEuvWNLii8/jBVnF8EiS"} 
        
        (function() {  // post to server
            
              var queueObj    = { 
                                        'offerName' :   queueData.offerName,            
                                        'resTime'   :   queueData.resTime,              
                                        'qTok'      :   queueData.queueTokenHash,      
                                        'myMac'     :   myDevice.mac,         
                                        'myService' :   {
                                                            'serviceType'  : queueData.serviceType,
                                                            'providerId'   : queueData.providerId,
                                                            'actionType'   : queueData.actionType,
                                                            'customNumber' : queueData.queueTokenHash
                                                        },
                                        'myPos'     :   { 
                                                            'myLoc'     :   null,       //  myQueues[0].myPos.myLoc.lat  & lng                       
                                                            'myEta'     :   null,       //  myQueues[0].myPos.nyEta.dist    
                                                            'timeStamp' :   nowTime()   //  myQueues[0].myPos.timeStamp
                                                        }
                                    };

            
          /*   var queueObj    = { 
                                        'offerName' :   queueData.offerName,
                                        'resTime'   :   queueData.resTime,
                                        'qTok'      :   queueData.queueTokenHash,
                                        'myMac'     :   myDevice.mac,
                                        'myPos'     :   {
                                                            'myLoc'     :   { 
                                                                                'lat' : myLat, 
                                                                                'lng' : myLng
                                                                            },
                                                            'myEta'     :   {
                                                                                'myDist'    :   {
                                                                                                    'text'  :   null,    
                                                                                                    'value' :   null
                                                                                                },
                                                                                'myDur'     :   {
                                                                                                    'text'  :   null,
                                                                                                    'value' :   null
                                                                                                }
                                                                            },
                                                            'nowTime'   :   nowTime() 
                                        }
                                    };  */
            dataObj = queueObj;
        })();

        return dataObj;   
    },
    
    
    buildMyCheckinObj: function(queueData) {
        
        var dataObj = {};
        
        // Return data queueData = {"status":"queue saved","username":"gdewitt","offerName":"Coffee time","offerMarkerImageAddr":"img/traqin_hot_deal_3_64.png","queueMarkerImageAddr":"img/traqin_marker_64.png","resTime":"2017-08-16 22:23:04 +0000","queueTokenHash":"vQZWUeXRu9JKIgEoDezFqOV7XY2tQNHKYe2LRBZrwtkwAAHPpJNge","location":{"lat":36.0370545239789,"lng":-115.10198898647694},"acceptToken":"X.Hv0euBR1tR3R0pRreaMOwmcKVSh6EeOsEuvWNLii8/jBVnF8EiS"} 
        
        (function() {  // post to server
            
              var queueObj    = { 
                                        'offerName' :   queueData.offerName,            
                                        'resTime'   :   queueData.resTime,              
                                        'qTok'      :   queueData.queueTokenHash,      
                                        'myMac'     :   myDevice.mac,         
                                        'myService' :   {
                                                            'serviceType' : queueData.myService.serviceType,
                                                            'providerId'  : queueData.myService.providerId,
                                                            'actionType'  : queueData.myService.actionType,
                                                            'customNumber' : queueData.queueTokenHash
                                                        },
                                        
                                        'myPos'     :   { 
                                                            'myLoc'     :   null,       //  myQueues[0].myPos.myLoc.lat  & lng                       
                                                            'myEta'     :   null,       //  myQueues[0].myPos.nyEta.dist    
                                                            'timeStamp' :   nowTime()   //  myQueues[0].myPos.timeStamp
                                                        }
                                    };

            
          /*   var queueObj    = { 
                                        'offerName' :   queueData.offerName,
                                        'resTime'   :   queueData.resTime,
                                        'qTok'      :   queueData.queueTokenHash,
                                        'myMac'     :   myDevice.mac,
                                        'myPos'     :   {
                                                            'myLoc'     :   { 
                                                                                'lat' : myLat, 
                                                                                'lng' : myLng
                                                                            },
                                                            'myEta'     :   {
                                                                                'myDist'    :   {
                                                                                                    'text'  :   null,    
                                                                                                    'value' :   null
                                                                                                },
                                                                                'myDur'     :   {
                                                                                                    'text'  :   null,
                                                                                                    'value' :   null
                                                                                                }
                                                                            },
                                                            'nowTime'   :   nowTime() 
                                        }
                                    };  */
            dataObj = queueObj;
        })();

        return dataObj;   
    },
    
    
    buildSearchObj: function(searchData) {
        
        var dataObj = {};
        
        // Return data searchData = {"status":"queue saved","username":"gdewitt","offerName":"Coffee time","offerMarkerImageAddr":"img/traqin_hot_deal_3_64.png","queueMarkerImageAddr":"img/traqin_marker_64.png","resTime":"2017-08-16 22:23:04 +0000","queueTokenHash":"vQZWUeXRu9JKIgEoDezFqOV7XY2tQNHKYe2LRBZrwtkwAAHPpJNge","location":{"lat":36.0370545239789,"lng":-115.10198898647694},"acceptToken":"X.Hv0euBR1tR3R0pRreaMOwmcKVSh6EeOsEuvWNLii8/jBVnF8EiS"} 
        
        
        (function() {  // post to server
            
            var searchObj =     {
                                        'myService' :    searchData,   
                                                        //{   
                                                         //   'serviceType' : searchData.serviceType, // searchData.myService.serviceType
                                                         //   'providerId'  : searchData.providerId,
                                                         //   'actionType'  : searchData.actionType,
                                                         //   'serviceData' : searchData.serviceData.query // { 'query' : searchData.query }
                                                       // },
                                        
                                        'myPos'     :   { 
                                                            'myLoc'     :   myLoc,       //  myQueues[0].myPos.myLoc.lat  & lng                       
                                                            'myEta'     :   myEta,       //  myQueues[0].myPos.nyEta.dist    
                                                            'timeStamp' :   nowTime()   //  myQueues[0].myPos.timeStamp
                                                        }
                                }
              

            dataObj = searchObj;
        })();

        return dataObj;   
    },
    
    
    
    buildPosObj: function(posData) {   //  route distance and duration data
        var queuePosData = {};
        (function() {
            
              var posObj    =  {
                                    'lat' :   posData.lat,    
                                    'lng' :   posData.lng
                                };
                  
            queuePosData = posObj;
        })();

     //   alert('queuePosData.myPos.lat is: '+queuePosData.lat);
        return queuePosData;
    },  
    
    
    buildEtaObj: function(routeData) {   //  route distance and duration data
       // var etaData = routeData,
        var queueEtaData = {};
        (function() {
            
              var etaObj    =   {
                                    'myDist'    :   {
                                                        'text'  :   routeData.routes[0].legs[0].distance.text,    
                                                        'value' :   routeData.routes[0].legs[0].distance.value
                                                    },
                                    'myDur'     :   {
                                                        'text'  :   routeData.routes[0].legs[0].duration.text,
                                                        'value' :   routeData.routes[0].legs[0].duration.value
                                                    },
                                };

            queueEtaData = etaObj;
        })();

      //  alert('queueEtaData.myDist.text is: '+queueEtaData.myDist.text);
        return queueEtaData;
    },     
    
    myLoc: function() {
        
        // update current position
        
        
        
            return { 'lat' : myLat, 'lng' : myLng}; 
    }, 
    

    
    venLoc: function() {
        
            return {lat: currentOffer.lat, lng: currentOffer.lng};  
    },  
    
    myEta: {
    
    },
    
    myDist: function(lat,lng) {
        
            myLat = lat;  // Store
            myLng = lng;  // Store
        
            return locate.relativePosition( 
                                                     '',
                                                     currentOffer.lat,
                                                     currentOffer.lng,
                                                     lat,
                                                     lng
                                            ).new;  

                
    },
    
      /*  queueObjBuilder: function(queueData) {   //  queueData
        
        var dataObj = dataObject.buildMyQueueObj(queueData);
        
        return dataObj;
    },  */
    
 
    

        
   /* queue: function(queueObj) {
        
        var myQueueObj = queueObj;
        
        (function(){ 
        
            var myObj       = {
                
            }
            myQueueObj = myObj;
                
        })();  
            
            myQueues.push(myQueueObj);
        
    }        */   
    
    
    envelope: function(obj) {  // type of data to server, load details in envelope   dataObj.envelope({type: 'queueCheckinPost' , details: tokenInfo}):

        // {type: 'queueCheckinPost' , details: tokenInfo}, {type: 'queuePosPost' , details: posInfo}, {type: 'AcceptOfferPost' , details: offerAcceptObj }
          
            return { 
                             "version":"1.0",
                             "secret":"zonetraq",
                             "type": obj.type,        
                             "data":
                                    {
                                      "apMac": '',
                                      "apTags": [''],
                                      "apFloors": [''],
                                      "details":  [
                                                        obj.details
                                                  ]
                                    }
            }; 
            
        }
    
},   


/*
*
*
* VENUEDETAIL CONTROLLER
*
*
*/

    
VenueDetail = {      

       friendId         : null,
       gMap             : null,
       map              : null,
       mapCenter        : null,
       venueData      : null,
       currentPosition  : null,

       init: function(currentOfferData) {  //
     //       alert("I'm inside Jim!" + venue['lat'] + venue['lng']);
          var me = this;

            me.venueData  = currentOfferData;  // currentOfferData data is the same as in currentOffer global variable
            console.log("typeof for VenueDetail.venueData['lat'] is: " + typeof me.venueData['lat']);
            me.mapCenter    = new google.maps.LatLng (me.venueData['lat'], me.venueData['lng']);
            me.initMap();
            me.statusControls();  

    /*      FriendsWithBeerDB.runQuery("select friend.*, beer.name beername from friend LEFT JOIN beer on friend.beerid = beer.id where friend.id = " + this.friendId, function(records) {
            me.venueData = records[0];
            me.mapCenter = new google.maps.LatLng (records[0].lat, records[0].lng);
            me.initMap();
            me.outputDetails();
            me.configureButtons();
            me.outputDistance();
          }); */
       },
    
       statusControls: function() {
           
            var me = this;
           
            me.outputDetails();
            me.configureButtons();           
            me.outputDistance();  
           
       },

       configureButtons: function() {

       //    alert("Im in configureButtons");

          $("#btnPhone").bind("tap", function(e) {
              var telLink = "tel:" + VenueDetail.venueData.phone;
              location.href=telLink;
            //  Notifications.init();
          });

          $("#btnMail").bind("tap", function(e) {
              var mailLink = "mailto:{0}?subject={1}&body={2}";
              mailLink = mailLink.format(
                  VenueDetail.venueData.email,
                  escape("Can I come over now?"),
                  escape("Hey good buddy, can I come over? Need to chat with you about some stuff!")
              );
              location.href = mailLink;
          });

          $("#btnNav").bind("tap", function(e) {
                var mapsUrl = "http://maps.google.com/maps?daddr={0},{1}&saddr={2},{3}";
                var url = mapsUrl.format(
                      VenueDetail.venueData.lat,                  // currentOffer.lat
                      VenueDetail.venueData.lng,                  // currentOffer.lng
                      VenueDetail.currentPosition.lat(),            // myLat
                      VenueDetail.currentPosition.lng()             // myLng
                 );
              
              if( /Android/i.test(navigator.userAgent) ) {

                 window.open(url, '_blank', 'location=yes');

                  // clickLink(mapsUrl);

                  /*
                  window.plugins.webintent.startActivity(
                     {
                      action: window.plugins.webintent.ACTION_VIEW,
                      url: 'geo:0,0?q=1400 16th st NW 20036'
                     },
                     function() {},
                     function() {alert('Failed to open URL via Android Intent')}
                   );

                  return;
                  */

              } else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {

                   mapsUrl = "maps://?daddr={0},{1}&saddr={2},{3}";

                   url = mapsUrl.format(
                      VenueDetail.venueData.lat,
                      VenueDetail.venueData.lng,
                      VenueDetail.currentPosition.lat(),
                      VenueDetail.currentPosition.lng()
                    );

                  location.href = url;
              } else {
                navigator.notification.alert("Not supported");
              }

          });

         $("#btnTwitter").bind("tap", function(e) {
           //  Notifications.init();
             navigator.notification.alert('No twitter feed');                       
      /*      var html = <a class="twitter-timeline"  href="https://twitter.com/gtdewitt" data-widget-id="547106134796951553">Tweets by @gtdewitt</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)      [0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

             $("#notifications").html(html) ;  */
          });                                              



       },

       outputDetails: function() {
        //   alert("I'm in outputDetails " + venue['lat']);
          var me = this;
          var tplVenue = $("#tplVenue").html();

          var html = tplVenue.format(
            me.venueData.name,
            me.venueData.address,
            me.venueData.phone,
            me.venueData.name
          );
           console.log(tplVenue);
        //   html = "<div class=\"contactname\">"+venue['name']+"</div><div class=\"contactaddress\">"+venue['address']+"</div><div class=\"contactphone\">"+venue['phone']+"</div><div class=\"venuedistance\">"+venue['name']+" is only <span id=\"venuedistance\"></span> away!</div>";

           console.log(html);
           $("#venuedetails").html(html);
       },
    
    
    getCurrentPosition: function() {
                   
           var me = this;
           navigator.geolocation.getCurrentPosition(

             function(position) {
                me.currentPosition = new google.maps.LatLng(       //  VenueDetail.currentPosition
                    position.coords.latitude,
                    position.coords.longitude
                );
                 myLat = position.coords.latitude;
                 myLng = position.coords.longitude;
                 myLoc       = dataObject.myLoc();

             },

            function (error) {
               navigator.notification.alert("An error occurred getting my position");
            }
           );         
          
       },

      outputDistance: function() {
                   
           var me = this;
           navigator.geolocation.getCurrentPosition(

             function(position) {
                me.currentPosition = new google.maps.LatLng(       //  VenueDetail.currentPosition
                    position.coords.latitude,
                    position.coords.longitude
                );
                    miles = dataObject.myDist(position.coords.latitude,position.coords.longitude);

                $("#venuedistance").html(miles + " miles ");
                return miles;
             },

            function (error) {
               navigator.notification.alert("An error occurred");
            }
           );         
          
       },

       initMap: function() {
         var $mapContainer  = $("#map2"),
             me             = this;
                this.gMap = $mapContainer.gmap({  // this.gMap = $mapContainer.gmap({
                center: this.mapCenter,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                callback: function(map) {
                me.map = map;
                var trafficLayer = new google.maps.TrafficLayer();
                trafficLayer.setMap(map);  
                var panoramioLayer = new google.maps.panoramio.PanoramioLayer();
                panoramioLayer.setMap(map);  
              //  ctMarker();

                var     point = me.map.getCenter(),
                       
                        image   = me.venueData.offerMarkerImageAddr,
                        titles  = me.venueData.name,
                    
                        tplOffer = $("#tplOffer").html(),
                            
                        offerInfo = tplOffer.format(
                            me.venueData.offerName,
                            me.venueData.distance,
                            me.venueData.offerId,
                            me.venueData.offerType,
                            me.venueData.description
                          ),                       
                            
                
                markerData = {point: point, info: offerInfo, mapping: map, image: image , imageScale: 80, titles: titles, infoWin: true};
                cityMapper.createMarker(markerData);  // function createMarker(point, info, mapping, image, titles)   createMarker(point, info, me.map,image ,titles )
             //   alert('Point in initMap is: '+point);
                console.log('Device data: '+me.venueData.offerId);
                           
              //  alert('deviceInfo.offerId is: ' + me.venueData.offerId);
             }
          });

              $mapContainer.height ($("body").height() - 200); 
              setTimeout(function() {$('#map2').gmap('refresh');},500);    //  error Implied eval. Consider passing a function instead of a string (W066)   // I tried this but just made the map larger - google.maps.event.trigger(MapInstance,'resize')       
             
        },

        getFormData: function() {
           navigator.notification.alert('Need to develope this VenueDetail.getFormData function');
        },
    
        showDetails: function() {

            var detailHtml = '<div class="grid grid-pad urow uib_row_1 row-height-1 ui-grid-a" data-uib="layout/row" >' +
                  '<div class="col uib_col_4 col-0_9-12" data-uib="layout/col">' +
                    '<div class="widget-container content-area vertical-col ui-block-a" style="width:80%" id="venuedetails">' +
                      '<span class="uib_shim"></span>' +
                    '</div>' +
                  '</div>' +
                  '<div class="col uib_col_1 col-0_3-12" data-uib="layout/col">' +
                    '<div class="widget-container content-area vertical-col ui-block-b" style="width:20%">' +
                        ' <a class="widget uib_w_10 d-margins ui-btn ui-corner-all ui-shadow ui-icon-phone  ui-mini ui-btn-icon-notext" data-uib="jquery_mobile/button" data-role="button" data-mini="true" data-icon="phone" ' +
                        ' data-iconpos="notext" id="btnPhone">Tel</a> ' +
                            '<a class="widget uib_w_11 d-margins ui-btn ui-corner-all ui-shadow ui-icon-mail  ui-mini ui-btn-icon-notext" data-uib="jquery_mobile/button" ' +
                        ' data-role="button" data-mini="true" data-icon="mail" data-iconpos="notext" id="btnMail">email</a> ' +
                            '<a class="widget uib_w_12 d-margins ui-btn ui-corner-all ui-shadow ui-icon-navigation  ui-mini ui-btn-icon-notext" ' +
                        ' data-uib="jquery_mobile/button" data-role="button" data-mini="true" data-icon="navigation" data-iconpos="notext" ' +
                        ' id="btnNav">Nav</a> ' +
                        '<a class="widget uib_w_13 d-margins ui-btn ui-corner-all ui-shadow ui-icon-comment  ui-mini ui-btn-icon-notext" ' +
                        ' data-uib="jquery_mobile/button" data-role="button" data-mini="true" data-icon="twitter" data-iconpos="notext" ' +
                        ' id="btnTwitter">Twitter</a> ' +
                    '<span class="uib_shim"></span> ' +
                    '</div>' +
                  '</div>' +
                  '<span class="uib_shim"></span>' +
                '</div>' +
                '<div class="grid grid-pad urow uib_row_2 row-height-2 ui-grid-solo" data-uib="layout/row   > ' +
                  '<div class="col uib_col_2 col-0_12-12" data-uib="layout/col"> ' +
                    '<div class="widget-container content-area vertical-col  ui-block-c" style="width:100%" id="map2"> ' +
                      '<span class="uib_shim"></span> ' +
                    '</div>' +
                  '</div>' +
                  '<span class="uib_shim"></span> ' +
                '</div> ' ;

             $("#detail").html(detailHtml);

        }

    
},
    

    
offer = {
    
    
        loadPin: function(iOffer) {
            
                            var point                       = {},
                             //   iOffer                      = offerData.data.offers[i],
                             //   info,
                                brandName                   = helper.escaped(iOffer.brand);

                            point['name']                   = brandName +" ("+iOffer.floor+")";
                            point['id']                     = iOffer.id;
                            point['lat']                    = iOffer.lat;
                            point['lng']                    = iOffer.lng;
                            point['address']                = iOffer.brandAddress;
                            point['phone']                  = (iOffer.brandTel)             ? iOffer.brandTel             : null;
                            point['email']                  = (iOffer.brandEmail)           ? iOffer.brandEmail           : null;
                            point['twitter']                = (iOffer.brandTwitter)         ? iOffer.brandTwitter         : null;
                            point['facebook']               = (iOffer.brandFacebook)        ? iOffer.brandFacebook        : null;
                            point['prodOrServ']             = (iOffer.prodOrServ)           ? iOffer.prodOrServ           : null;
                            point['prodOrServName']         = (iOffer.prodOrServName)       ? iOffer.prodOrServName       : null;
                            point['prodOrServNun']          = (iOffer.prodOrServNun)        ? iOffer.prodOrServNun        : null;
                            point['offerId']                = (iOffer.offerId)              ? iOffer.offerId              : null;
                            point['offerType']              = (iOffer.offerType)            ? iOffer.offerType            : null;
                            point['description']            = (iOffer.description)          ? iOffer.description          : null;
                            point['offerName']              = (iOffer.offerName)            ? iOffer.offerName            : null;
                            point['startDate']              = (iOffer.startDate)            ? iOffer.startDate            : null;
                            point['stopDate']               = (iOffer.stopDate)             ? iOffer.stopDate             : null;
                            point['offerMarkerImageAddr']   = (iOffer.offerMarkerImageAddr) ? iOffer.offerMarkerImageAddr : offerMarker;          
                            point['queueMarkerImageAddr']   = (iOffer.queueMarkerImageAddr) ? iOffer.queueMarkerImageAddr : queueMarker;
                            point['serviceType']            = iOffer.serviceType;     // i.e. generate and activate mobile room key
                            point['providerId']             = iOffer.providerId;      // i.e. service provider such as dormakaba
                            point['actionType']             = iOffer.actionType;      // service database action such as get wallet token
                            point['mobileUrl']              = (iOffer.brandUrl)             ? iOffer.brandUrl             : "";
                        //    point['index'] = i;
                            point['info'] = "Name: "+point['name']+               // compile info to reveal upon listview, mapview and ARview item click
                                            "\\nAddress: "+point['address']+
                                            "\\nPhone: "+point['phone']+
                                            "\\nMenu: "+point['mobileUrl'];

                            //  was foursquare point['referralId'] = data.response.venues[i].referralId;

                            pin.push(point);
            
        },
    
        clearPinArray: function() {
                pin = [];
        },
    
        accept: function(offerId){

                var me = this,

                    offerAcceptObj = me.buildAcceptObj(offerId);

             //   alert('offerAcceptObj.location.lat is: '+offerAcceptObj.location.lat); 
                
                if (!qEventFlow.qActive) hashIt.encrypt(offerAcceptObj);  // If first iteration create hash and post to server
                else {
                    
                 //   var offerAcceptEnv = dataObject.envelope({type: 'AcceptOfferPost' , details: offerAcceptObj});
                    var offerAcceptEnv = me.buildAcceptEnv(offerAcceptObj);   // place in server object wrapper
                    server.postTo(offerAcceptEnv);  // output to server  was offerAcceptObj
                    
                }

        },
    
    
        buildAcceptObj: function(offerId) { 

            myProfile =  dataObject.myProfile();  // Get profile data object    dataObject.myProfile();  user.myProfile.buildObj();
            myDevice =  dataObject.myDevice();    // Get device data object     dataObject.myDevice();   user.myDevice.buildObj();

            var myAcceptObj =  {};

            (function(){

                var offerAcceptObj = dataObject.myOffer(offerId);

                myAcceptObj = offerAcceptObj;  
                //     alert('offerId in offer.buildAcceptObj is: '+offerAcceptObj.offerId); 
             })();  

           //  alert('myAcceptObj.offerId is: '+myAcceptObj.offerId); 
            return myAcceptObj;
         },

        buildAcceptEnv: function(offerAcceptObj) { 

            var offerAcceptEnv =  {};

            (function(){

             var offerEnv = dataObject.envelope({type: 'AcceptOfferPost' , details: offerAcceptObj});

              /*   {
                     "version":"1.0",
                     "secret":"zonetraq",
                     "type":"AcceptOfferPost",
                     "data":
                            {
                              "apMac": '',
                              "apTags": [''],
                              "apFloors": [''],
                              "details":  [
                                             offerAcceptObj
                                            ]
                            }
                  };   */
                offerAcceptEnv =  offerEnv;
            })();  

            return offerAcceptEnv;
        },
    
        clearOfferStack: function() {

            myOffers        = [];
            myQueues        = [];
            currentOffer    = {};
            currentQueue    = {};

        }
    
},
        
 
queue = {      
    
                // get the location
                doPosUpdate: function() {  //  { 'mac':null, 'lat':null, 'lng':null, 'dist':null, 'eta':null }
                   // alert("currentOffer['index') is "+ currentOffer['index'])
                   // qEventFlow.queueActive.On(); //  qEventFlow.qActive = true;  qEventFlow.queueActive.Off();
                     qEventFlow.setSendEta.On();  // sendEta = true; 
                    console.log('qEventFlow.setSendEta.On in doPosUpdate is: '+qEventFlow.sendEta);
                    
                //    routeMap.clearOverlays();


                  //  alert('In doPosUpdate myEta is: ' + myQueues[0].myPos.myEta.myDur.text );
                    if (myQueues.length > 0) {
                     //   alert('In doPosUpdate myEta is: ' + myQueues[0].myPos.myEta.myDur.text );  // myQueues[currentOffer.index].myPos.myEta
                        queue.initRouteMap(myQueues[0]);  // get new position, ETA and nowTime updates & set route on map  , myQueues[0]
                        console.log('Going to server with position update');

                    }

                }, 

                initRouteMap:  function(queueObj) {  // queueObjData  was queueObj
                    
                    (function() {
                          
                        var me             = this,
                            $mapContainer  = $("#map2"),
                            
                            myLoc       = dataObject.myLoc(),    // origin          { 'lat' : myLat, 'lng' : myLng},      
                            venueLoc    = dataObject.venLoc();   // destination 
                        
                        if (!qEventFlow.sendEta) {   // create global map once  
                            routeMap = new google.maps.Map(document.getElementById('map2'), {    // was local variable map
                              center: myLoc,
                              scrollwheel: false,
                              zoom: 12,
                              icon: currentOffer.queueMarkerImageAddr  // this doesn't work - can routes support customer markers?
                            });
                        } else { directionsDisplay.setMap(null); }   // clear old before diaplaying new route

                        directionsDisplay = new google.maps.DirectionsRenderer({
                              map: routeMap                                                      // was map
                        });

                        // Set destination, origin and travel mode.
                        var request = {
                          origin: myLoc,
                          destination: venueLoc,
                          travelMode: 'DRIVING'
                        },

                        // Pass the directions request to the directions service.
                        directionsService = new google.maps.DirectionsService();
                        
                        //    queueObj.myPos.myLoc        = myLoc;                                     // load myLoc info into object
                        
                        directionsService.route(request, function(response, status) {
                            if (status == 'OK') {
                              // Load the queue object and display the route on the map.
                                
                            /*    queueObj.myDist.text    = response.routes[0].legs[0].distance.text
                                queueObj.myDist.value   = response.routes[0].legs[0].distance.value;
                                queueObj.myDur.text     = response.routes[0].legs[0].duration.text;
                                queueObj.myDur.value    = response.routes[0].legs[0].duration.value;   */
                         //       var updateQueueObj = queue.routeUpdate(response);
                                
                                // position updated atributes: myLoc, myEta, timeStamp
                                queueObj.myPos.myLoc        = dataObject.buildPosObj(myLoc);
                                queueObj.myPos.myEta        = dataObject.buildEtaObj(response);  // etaObj;  // load myEta info into object
                                queueObj.myPos.timeStamp    = nowTime();                       // load timeStamp into object
                                myQueues[0] = queueObj;
                            //   myQueues.push(queueObj);    // push active queue to queue stack                          
                                
                                queue.showStatus(queueObj);                                    // was queue.showStatus(data); 
                                
                           //     alert('myQueues[0].myPos.myEta.myDist.text in directions method is: '+myQueues[0].myPos.myEta.myDist.text);
                                
                                directionsDisplay.setDirections(response);
                                console.log('qEventFlow.sendEta in routUpdate is: '+qEventFlow.sendEta);
                                if (qEventFlow.sendEta) queue.posUpdate(queueObj);  // Send to server - qEventFlow.qActive is temp turned off by 
                                
                            }
                            else  $("#error").append("Unable to retrieve your route<br />");
                        },
                        {
                            enableHighAccuracy: true,
                            timeout: 10 * 1000 // 10 seconds
                        }
                        );
                        $mapContainer.height ($("body").height() - 200); 
                        setTimeout(function() {$('#map2').gmap('refresh');},500); 
                    })();
                        
                     /*   alert('queueObj.myDist.text in directions is: '+queueObj.myDist.text);
                        queueObj.myDist.text    = queueObj.routes[0].legs[0].distance.text;
                        queueObj['myDist']['value']   = queueObj.routes[0].legs[0].distance.value;
                        queueObj['myDur']['text']     = queueObj.routes[0].legs[0].duration.text;
                        queueObj['myDur']['value']    = queueObj.routes[0].legs[0].duration.value;
                        queueObj['myLoc']             = myLoc; */
                      //  alert('qqueueObj.myDist.text is: '+queueObj.myDist.text);

                    //    return queueObj;

                },  
    
         /*       routeUpdate: function(queueObj){
                    var queueObjData = queueObj;
                    (function() {     
                        
                        queueObj.myDist.text    = queueObjData.routes[0].legs[0].distance.text;
                        queueObj.myDist.value   = queueObjData.routes[0].legs[0].distance.value;
                        queueObj.myDur.text     = queueObjData.routes[0].legs[0].duration.text;
                        queueObj.myDur.value    = queueObjData.routes[0].legs[0].duration.value;
  
                        console.log('myMac: ' + queueObjData.myMac);
                        console.log('.myPos.myLoc.lat: ' + queueObjData.myPos.myLoc.lat);
                        console.log('myPos.myLoc.lng: ' + queueObjData.myPos.myLoc.lng);
                        console.log('response routes myDist text: ' + queueObjData.myPos.myEta.myDist.text); // 
                        console.log('response routes myDist value: ' + queueObjData.myPos.myEta.myDist.value);
                        console.log('response routes myDur value: ' + queueObjData.myPos.myEta.myDur.value);
                        console.log('timeStamp: ' + queueObjData.myPos.timeStamp);
                     })();
                },    */
    
                posUpdate: function(queuePos) {
                    
                     //   offerAcceptToken = myToken;   // store in global variable for future reference
                       var  posInfo = 	{
                                        //    offerId:  queueToken.offerId,
                                        //    acceptToken: queueToken.acceptToken,
                                            position: queuePos
                                        },

                        posObject = dataObject.envelope({type: 'queuePosPost' , details: posInfo});
                           
                         /*  {
                                         "version":"1.0",
                                         "secret":"zonetraq",
                                         "type":"queuePosPost",
                                         "data":
                                                {
                                                  "apMac": '',
                                                  "apTags": [''],
                                                  "apFloors": [''],
                                                  "details":  [
                                                                 posInfo
                                                              ]
                                                }
                                      };    */
                        qEventFlow.queueActive.Off();  //  qEventFlow.qActive = false;
                        server.postTo(posObject);
                        
                },
    
                showStatus: function(queueDataObj) { 
                    var dataObj = queueDataObj,
                        me = this;
 
                    (function() {

     //   {   
     //        "offerName":"Coffee Time",
     //        "resTime":"2017-06-05 00:51:18 +0000",
     //        "queueToken":"$2a$10$5rrEURLW06FXf6UpLoX9pu/M9/mxMWAaWf.UHsztggdJIEjOAmbTy",
     //        "location":{"lat":36.04016178016024,"lng":-115.10302973203902},
     //        "acceptToken":"oUdY2YfR.XJd/QrKnS/bduH/LTmHgxlsF7YmR/Xrc9H0ndL5pPjkC"
     //    }
                    
                        console.log('myMac: ' + dataObj.myMac);
                        console.log('myLoc.lat: ' + dataObj.myPos.myLoc.lat);
                        console.log('myLoc.lng: ' + dataObj.myPos.myLoc.lng);
                        console.log('response routes myDist text: ' + dataObj.myPos.myEta.myDist.text); // 
                        console.log('response routes myDist value: ' + dataObj.myPos.myEta.myDist.value);
                        console.log('response routes myDur text: ' + dataObj.myPos.myEta.myDur.text);
                        console.log('response routes myDur value: ' + dataObj.myPos.myEta.myDur.value);
                        console.log('timeStamp: ' + dataObj.myPos.timeStamp);
                    
                        var tplQueue = $("#tplQueue").html(),

                            html = tplQueue.format(
                                currentOffer.name,
                                dataObj.offerName,                    // was queueDataObj.offerName,  was myOffers[0].name,
                                dataObj.resTime,                      // was queueDataObj.resTime, was  myQueues[0].resTime,   
                                dataObj.myPos.myEta.myDist.text,      // was queueDataObj.myPos.myEta.myDist.text, was myQueues[0].myPos.myEta.myDist.text, 
                                dataObj.myPos.myEta.myDur.text,       // was queueDataObj.myPos.myEta.myDur.text,  was myQueues[0].myPos.myEta.myDur.text,  
                                dataObj.qTok                          // was queueDataObj.qTok  was  myQueues[0].qTok 
                              );
                    
                    //    alert('myDist.text is: '+ dataObj.myPos.myEta.myDist.text);

                        $("#venuedetails").html(html); 
                      //  queueData = {info: data};
                        appPage.queueView();
                      //  myPosObj = dataObj;
                    })();

                },
    
                cleanup: function() {      // clear offer and queue variables and active queue on server
                    
                                        
                        qEventFlow.setSendEta.Off(); //      sendEta = false;        // turn off eta to server
                        qEventFlow.queueActive.Off();    //   qEventFlow.qActive = false;        // turn off queue activities
                        currentOffer = {};      // empty current offer selected variable

                       var  queueInfo = 	{
                           
                                            qDelTok: currentQueue.qTok
                                        },

                        qDelObject = dataObject.envelope({type: 'queueDelPost' , details: queueInfo});
                           
                         /*  {
                                         "version":"1.0",
                                         "secret":"zonetraq",
                                         "type":"queueDeletePost",
                                         "data":
                                                {
                                                  "apMac": '',
                                                  "apTags": [''],
                                                  "apFloors": [''],
                                                  "details":  [
                                                                 queueInfo
                                                              ]
                                                }
                                      };    */
                     //   alert('currentQueue.qTok in queue.cleanup is: '+currentQueue.qTok);
                        server.postTo(qDelObject);
                    
                },
    
                doToggle: function(notices) {  // Not used at present
                          $(  "#map2" ).fadeToggle( "slow", "linear", function() {
                                $( "#queue" ).fadeToggle( "slow", "linear" );
                                $( "#notice" ).append( "<div>" + notices + "</div>" );
                          });
                }

},


hashIt = {  // { deviceUuid: uuid, timeStamp: timeStamp },

            encrypt : function(offerAcceptObj) {
                var uuid    = offerAcceptObj.uuid,           // was pre wrapped  offerAcceptObj.data.details[0].uuid,
                    rnds        = 8,
                    uniqueSalt        = offerAcceptObj.timeStamp,  // .toString()   was prewrappted offerAcceptObj.data.details[0].timeStamp;
                  //  uniqueSalt = ""
                    password    = uuid + uniqueSalt;     // store for checkin verification
                    offerSeed    = password;
                 //   alert('salt in hashit is: '+salt);
                console.log('offerSeed in hashIt.encrypt is: ' + offerSeed);
                  // ... your code, then:
              //    var bcrypt = new bCrypt();
              /*    bcrypt.hashpw('plain password', bcrypt.gensalt(), function (hashed) {
                      var cipher = AES(SHA1(hashed));
                      // now you have your "secure" hash
                  });  */
                 hashpw(
                    password,
                    gensalt(rnds),
                    function(h) { 

                                    var offerAcceptToken = h;
                                    var offerAcceptTokenArray = offerAcceptToken.split("$");   // hashArray[3] has the hashed item in it
                                    offerAcceptTokenKey = [ '$', offerAcceptTokenArray[1], '$', offerAcceptTokenArray[2], '$'].join("");   // no seperator
                                    offerAcceptObj.acceptToken = offerAcceptTokenArray[3];  
                                    offerAcceptTokenHash = offerAcceptTokenArray[3];
                        
                                    console.log('offerAcceptToken in hashIt.encrypt is: '+ offerAcceptToken);
                                    console.log('offerAcceptTokenArray length is: ' + offerAcceptTokenArray.length);  //  = 4
                                    console.log('offerAcceptTokenArray[0] is: ' + offerAcceptTokenArray[0]);    // empty
                                    console.log('offerAcceptTokenArray[1] is: ' + offerAcceptTokenArray[1]);    // 2a                       algorithm version
                                    console.log('offerAcceptTokenArray[2] is: ' + offerAcceptTokenArray[2]);    // 08                       rounds number
                                    console.log('offerAcceptTokenArray[3] is: ' + offerAcceptTokenArray[3]);    // offerAcceptTokenKey...   hash
                                    console.log('offerAcceptTokenKey is: ' + offerAcceptTokenKey);
                        
                                    myQueueReq = offerAcceptObj;     // This is what I sent to offerTraQ to request a queue    
                        
                        
                                  //  offerAcceptEnv = dataObject.envelope({type: 'AcceptOfferPost' , details: offerAcceptObj});
                                    offerAcceptEnv = offer.buildAcceptEnv(offerAcceptObj);   // place in server object wrapper
                                    server.postTo(offerAcceptEnv);  // output to server  was offerAcceptObj
                                    qEventFlow.queueActive.On();  // qEventFlow.qActive = true;
                                },
                    function() {}
                 );
            },
        
            verify: function(serverTokenData) {   // Verify token is valid
                var me = this,
                acceptTokHash = serverTokenData.data.acceptTokHash;
               // alert('This is acceptTokHash: '+acceptTokHash)
                console.log('offerSeed in hashIt.verify is: ' + offerSeed);
                
                var offerAcceptTokenBuild = [ offerAcceptTokenKey, acceptTokHash].join("");
               // alert('This is offerAcceptTokenBuild: '+offerAcceptTokenBuild)
                if (acceptTokHash !== null) {
                  checkpw(
                        offerSeed,    //  original token password
                        offerAcceptTokenBuild,                                   // offerAcceptToken   acceptToken.queueToken   last was offerAcceptToken
                        function(h) {
                                        if (h) {
                                                  //  alert('serverTokenData.status is : ' + serverTokenData.status);
                                                  
                                                    checkin.show(serverTokenData);
                                                    var checkinObj = dataObject.buildMyCheckinObj(serverTokenData.data);
                                                    myCheckin = checkinObj;
                                                    if (serverTokenData.data.myService.serviceType == "room") {
                                                         
                                                         if (serverTokenData.data.myService.actionType == "getRoomKey") {
                                                             var serviceObj = {'myService' : checkinObj.myService }
                                                             service.do(serviceObj);   // Need to add serviceType, providerId, actionType to data
                                                         }
                                                    }                                           
                                                    
                                        } else {
                                                    checkin.error(serverTokenData);
                                                //    return false;
                                        }
                                    },
                        function() {}
                  );  
                } else navigator.notification.alert('offerAcceptToken in hashIt.verify is : ' + offerAcceptToken);
            }
    

},
    
checkin = {
    
                do: function(queueToken) {
                    
                     //   offerAcceptToken = myToken;   // store in global variable for future reference
                       var  tokenInfo = 	{
                                        //    offerId:  queueToken.offerId,
                                        //    acceptToken: queueToken.acceptToken,
                                            queueToken:     queueToken,
                                            serviceType:    currentQueue.serviceType
                                        },

                        tokenObject = dataObject.envelope({type: 'queueCheckinPost' , details: tokenInfo});
                           
                           
                       /*    {
                                         "version":"1.0",
                                         "secret":"zonetraq",
                                         "type":"queueCheckinPost",
                                         "data":
                                                {
                                                  "apMac": '',
                                                  "apTags": [''],
                                                  "apFloors": [''],
                                                  "details":  [
                                                                 tokenInfo
                                                                ]
                                                }
                                      };   */
                    
                        server.postTo(tokenObject);
                        
                },
    
    
                show : function(tokenStatus) {
                        qEventFlow.setSendEta.Off();  //  sendEta = false;
                        qEventFlow.queueActive.Off(); // qEventFlow.qActive = false;
               
                        

                        var tplCheckin = $("#tplCheckin").html(),
                            uName = myProfile.firstName + ' ' + myProfile.lastName,
                            offerName = myOffers[0].offerName,
                            distance = VenueDetail.outputDistance(),
                            
                       
                    
                        html = tplCheckin.format(
                                uName,  // was myProfile.firstName & myProfile.lastName
                                offerName,  // tokenStatus.offerName  was myOffers[0].offerName,
                                tokenStatus.status,                                  
                                distance, 
                                "Room: 101"
                              ),
                            
                          welecomeImg =  '<div><img src="img/welcome_dewitt_splash.jpeg" alt="TraQin" style="float:left;width:100%;height:100%"></div>';
                    
                        console.log("<p>Node ID verification: Passed</p>");
                    //    alert('tokenStatus.status in showCheckin is: '+tokenStatus.status);
                     //   alert("<p>Checkin Verified</p></br><p>Welcome to Angel Cloud</p></br><p> "+ uName  +"</p>");
                      //  queueData = {info: data};
                
                      /*    $( function() {
                            $( "#dialog" ).dialog();
                          } );  */
                 /*       $.mobile.changePage( "#dialog", { 
                            modal: true,
                            transition:'pop',
                            role: "dialog",
                            changeHash: false
                        }));    */
                
                   /*     $(document).ready(function() {
                            $("#dialog").dialog({
                                modal: true
                            }).dialog("widget")
                              .next(".ui-widget-overlay")
                              .css("background", "#f00ba2");  
                        });  */
                     //   $('.ui-widget-overlay').css('background', 'blue');
                
                    /*    $("#dialog").dialog(
                        {
                            autoOpen: false, 
                            modal: true, 
                            open: function() {
                                $('.ui-widget-overlay').addClass('custom-overlay');
                            },
                            close: function() {
                                $('.ui-widget-overlay').removeClass('custom-overlay');
                            }            
                        });  */
                
                        $("#map2").html(welecomeImg);  // welcome splashscreen
                    /*    var url = dataSource.meshPoint.BASEURL+"angelcloud/www/index.html";   // turn this on to fire app
                        console.log("Property app url is:" + url);
                    
                        $("#map2").load(url, function () {
                            
                            console.log("We're in angel cloud app Jim");
                    
                        //    $('#formContent').load('createOfferForm.html', function () {  from original offertraq code
                        
                         // $('#autoSelectCode').load('autoSelectCodeNew.html');  // load create offer form html page
   //                       loadAutoSelectCode();   // load create offer form - auto field data html page
                       //   offer.createFormToggle();   
   //                       idObj = initialize.idBuilder(bIdValue,recordId);   // { 'bIdValue' : bIdValue ,'vIdValue' : vIdValue ,'fNameValue' : fNameValue }
   //                       initialize.toggleViews(idObj.bIdValue,idObj.vIdValue); // toggle create button group
   //                       if ($(idObj.vIdValue).is(":visible")) {
                           //  if (!allOffers.length) offers.findAll();                          // if not downloaded then fetch offers from database
   //                          $('#offerForm').trigger("reset");                                 // reset form to create new offer    
   //                          $("#brandName").focus();
    //                         console.log('Offer create on');
    //                      } else {
    //                        console.log('Offer create off');
    //                      }
                      
                      });          */           
                    

                        $("#venuedetails").html(html); 
                    
                        offer.clearOfferStack();
                    
                        qEventFlow.checkinWelcomeNotice();
                    
                       
        
            },
    
            error : function(tokenStatus) {
                console.log("<p>Node ID verification: Fail</p>");
                navigator.notification.alert('tokenStatus.status in showCheckin is: '+tokenStatus.status);

            }
    
    
    
},
    
    
service = {
    
                do: function(serviceData) {    // prepair and send service request to server
                     
                     //   var serviceData = data;

                        serviceObj = dataObject.envelope({type: 'webServicesPost' , details: serviceData});
    
                       /*                                   
                                      
                             "version":"1.0",
                             "secret":"zonetraq",
                             "type": obj.type,        
                             "data":
                                    {
                                      "apMac": '',
                                      "apTags": [''],
                                      "apFloors": [''],
                                      "details":  [
                                                        obj.details
                                                  ]
                                    }
                                      
                                      
                                      */
                    
                        server.postTo(serviceObj);
                        
                },
    
                show : function(tokenStatus) {  // present checkin details in mobile app top view
                    
                        qEventFlow.setSendEta.Off();  //  sendEta = false;
                        qEventFlow.queueActive.Off(); // qEventFlow.qActive = false;

                        var tplCheckin = $("#tplCheckin").html(),
                            uName = myProfile.firstName + ' ' + myProfile.lastName,
                            offerName = myOffers[0].offerName,
                            distance = VenueDetail.outputDistance(),
 
                        html = tplCheckin.format(
                                uName,  // was myProfile.firstName & myProfile.lastName
                                offerName,  // tokenStatus.offerName  was myOffers[0].offerName,
                                tokenStatus.status,                                    //  status is undefined
                                distance 
                              ),
                            
                          welecomeImg =  '<div><img src="img/welcome_dewitt_splash.jpeg" alt="TraQin" style="float:left;width:100%;height:100%"></div>';
                    
                        console.log("<p>Node ID verification: Passed</p>");
                
                        $("#map2").html(welecomeImg);  // welcome splashscreen
     

                        $("#venuedetails").html(html); 
                    
                        offer.clearOfferStack();
                    
                        qEventFlow.checkinWelcomeNotice();
        
            },
    
    
            room : {       // service.room.walletRegDeviceId(customNum);  
                 
             /*   walletRegDeviceId : function(customNum) {
                      var deviceId = "50122131-"+customNum;
                       console.log('in walletRegDeviceId method customNum is: '+deviceId);
                       window.plugins.toastyPlugin.register(deviceId, function () { 
                           console.log('Register custom number success!');
                       }, function (err) {
                           console.error('error registering custom number:', err);
                       });     
                },   */
                
                walletRegDeviceId : function(data) {
                     //  let deviceId = data.legicCustomNumber
                       var deviceId = "50122131-"+data.legicCustomNumber;
                   //    let deviceId = customNum;
                       var legicWalletToken = data.legicWalletToken
                       window.plugins.toastyPlugin.startRegistration(deviceId, function () { 
                           console.log('Register custom number success!');
                           service.room.walletRegDeviceToken(legicWalletToken);
                       }, function (err) {
                           console.error('error registering custom number:', err);
                       });     
                },
                
                
                walletRegDeviceToken : function(token) {      // service.room.finishRegistration(token);
                      var deviceToken = token;
                    console.log('in walletRegDeviceToken method token is: '+token);
                       window.plugins.toastyPlugin.finishRegistration(deviceToken, function () { 
                           console.log('Register Legic token success!');
                       }, function (err) {
                           console.error('error registering legic token:', err);
                       });    
                },
                
                legicKeyServerSync : function() {      // service.room.legicKeyServerSync();
                       window.plugins.toastyPlugin.synchronize(function () { 
                           console.log('Legic synchronize success!');
                           service.room.legicKeyServerGetFiles();
                       }, function (err) {
                           console.error('error synchronizing with Legic server:', err);
                       });    
                },
                
                legicKeyServerGetFiles : function() {      // service.room.legicKeyServerGetFiles();
                       window.plugins.toastyPlugin.getFiles(function (response) {
                            console.log('Legic get files success!');
                           if (response != null || response != "" || response != undefined) {
                               console.log("get_files response is: "+response);
                             //   var data = $.parseJSON(response);
                            //   var key = data.Index[0]
                              var key = 1
                                service.room.legicKeyServerGetCard(key);
                           } else console.log("No get_file data found");
                       }, function (err) {
                           console.error('error getting files from Legic server:', err);
                       });    
                },

                legicKeyServerGetCard : function(key) {      // service.room.legicKeyServerGetCard(key);
                       var index = key;
                       window.plugins.toastyPlugin.getCard(index,function () { 
                           console.log('Legic get card success!');
                       }, function (err) {
                           console.error('error getting card from Legic server:', err);
                       });    
                },

                walletUnregister : function() {      // service.room.walletUnregister();

                       window.plugins.toastyPlugin.unregister(function () { // was (index)
                           console.log('Legic wallet unregister success!');
                       }, function (err) {
                           console.error('error unregistering legic wallet:', err);
                       });    
                }

      

            },

            error : function(tokenStatus) { 
                console.log("<p>Node ID verification: Fail</p>");
                navigator.notification.alert('tokenStatus.status in showCheckin is: '+tokenStatus.status);

            }
   
},     
    
    


server = {  
    
            getData: function(query) { 
    
                var base    = dataSource.server.offertraqUrl, // "http://cmx-api-app-ruby-gtdewitt.c9users.io", "https://cloudtraq-cmx.herokuapp.com"
                    page    = "/offers/",
                //    jsonWithPadding     = "?alloworigin=false&callback=?",
                    url     = base + page,
                    url2    = "https://api.foursquare.com/v2/venues/search?query="+query+"&ll="+myLat+","+myLng+"&intent=checkin&limit=20&v=20130203&client_id="+FOURSQUARE_CLIENT_ID+"&client_secret="+FOURSQUARE_CLIENT_SECRET+"&callback=?";
                //    if (query = "!") url     = url1;
    
                $.getJSON(url, function(results){
                  //  alert('offers length is: '+results.data.offers.length);
                    data.manage(results);  
                  //  return data;
                   
                });
            
            },

            postTo: function(data) {     // accepting offer

                var me = this,
                    base    = dataSource.server.offertraqUrl, // "https://cloudtraq-cmx.herokuapp.com"  "http://cmx-api-app-ruby-gtdewitt.c9users.io"
                    page    = "/events",
               //     jsonWithPadding     = "?alloworigin=false&callback=?",   //https://cloudtraq-cmx.herokuapp.com/events
                    url     = base + page;
                //    offerId             = "ACCR1-CM-000001-0001-000001";
                //    console.log('url is : ' + url);

              jQuery(function($) {
                $.ajax({
                    type:           "POST",
                    url:            url,
                    processData:    false,
                    contentType:    "application/json",
                    data:           JSON.stringify(data),
                    success: function(response) {
                       
                        server.doPostResponse(response);
                        
                    },
                      error: function (XMLHttpRequest, textStatus, errorThrown) {
                          if (textStatus == 'Unauthorized') {
                              navigator.notification.alert('Unautherized - custom message. Error: ' + errorThrown);
                          } else {
                              navigator.notification.alert('custom message. Error: ' + errorThrown);
                          }
                      }
                  });
              });    
            },
    
            doPostResponse: function(response) {
                
                console.log('server.postTo response: '+response);
                
                var data = $.parseJSON(response);
                
                switch (data.status) {
                    case 'invalid':   // JSON data model version is not valid
                        navigator.notification.alert('Invalid data model version!');
                        break;
                // queue services    
                    case 'q_saved':    // begin queue management services
                        console.log('Server return status is: '+ data.status);
                        (function() {
                         //   alert('queue saved data.offerName: '+ data.offerName);
                            qEventFlow.setSendEta.Off();  // sendEta = false; Don't send eta untiil Queue session is established  
                            var queueObj = dataObject.buildMyQueueObj(data);  // build queue object base
                         //   alert('queueObj.actionType is: '+queueObj.myService.actionType);
                            if (myQueues.length > 0) myQueues = [];
                            myQueues.push(queueObj);    // push active queue to queue stack
                            currentQueue =queueObj;     // this is the current active Queue session
                            queue.initRouteMap(myQueues[0]);  //  was (queueObj) initiate the queue map, get and store position object
                            qEventFlow.queueActive.On();  // qEventFlow.qActive = true;parameters
                            console.log('qEventFlow.queueActive.On after q_saved is: '+qEventFlow.qActive);
                            if (data.actionType == "walletPreauth") service.do(queueObj);   // Need to add serviceType, providerId, actionType to data model
                          //  function myFunction() {
                            //    setInterval(function(){ queue.doPosUpdate();   }, 30000);
                          //  }
                        })();
                         //   var notices = 'Status: ' + data.status;
                         //   queue.doToggle(notices);            // toggle #map2 and #queue data views
                        break;
                    case 'q_updated':  // upate queue ETA with server (server method: updateQueueData(map))
                        qEventFlow.queueActive.On();  //  qEventFlow.qActive = false;
                        console.log('Server return status: '+ data.status+', '+data.message);
                        break;
                    case 'q_not_updated': // queue ETA not updated, Invalid queue token supplied. (server method: updateQueueData(map))
                        console.log('Server return status: '+ data.status+', '+data.message);
                    case 'q_deleted':          // verify checkin
                        console.log('Server return status is: '+ data.status+', '+data.message);
                        currentQueue = {};      // empty current offer selected variable  q_deleted
                        
                        break;      
                    case 'q_not_deleted':          // verify checkin
                        console.log('Server return status is: '+ data.status+', '+data.message);
                        break;
                // checkin services        
                    case 'q_mac_available': // Checkin requested - queue ETA updated and within threashold (server method: updateQueueData(map))
                        console.log('Server return status: '+ data.status+', '+data.message);
                      //  if (qEventFlow.qActive) {
                          //  qEventFlow.queueActive.Off();  //  qEventFlow.qActive = false;
                            qEventFlow.requestWirelessCheckin();  // Request quest accept wireless auto checkin proceedure option
                      //  }  
                        break; // q_mac_unavailable
                     case 'q_mac_unavailable': // queue ETA updated with server and within checkin threashold but mac unavailable
                        console.log('Server return status: '+ data.status+', '+data.message);
                       // if (qEventFlow.qActive) {
                          //  qEventFlow.queueActive.Off();  //  qEventFlow.qActive = false;
                            qEventFlow.requestNfcCheckin();  
                            //qEventFlow.manualCheckinRequest();  // Request accept wireless auto checkin proceedure option
                      //  }
                        break;                         
                    case 'checkin_verified':          // verify checkin
                        console.log('Server return status is: '+ data.status+', '+data.message);  // qEventFlow.qActive = false;
                     
                        hashIt.verify(data);                            //  var identified = hashIt.verify(data);
                      /*  var checkinObj = dataObject.buildMyCheckinObj(data.data);
                        if (data.data.actionType == "getRoomKey") {
                               alert('data.data.actionType is: '+data.data.actionType);
                            service.do(checkinObj);   // Need to add serviceType, providerId, actionType to data
                        }  */
                        //  if (identified) alert("Node ID verified");  else alert("Node ID cannot be verified"); 
                        break;
                    case 'checkin_not_verified':          // checkin is cannot be verified
                        console.log('Server return status: '+ data.status+', '+data.message);  //  
                        qEventFlow.queueActive.On();  //  qEventFlow.qActive = true;
                            //  var identified = hashIt.verify(data);
                        //  if (identified) alert("Node ID verified");  else alert("Node ID cannot be verified"); 
                        break;
                    case 'checkin_create_exception':          // checkin record created but queue not deleted
                        console.log('Server return status: '+ data.status+', '+data.message);  //  checkin_create_exception
                        break;
                        
                    case 'wallet-preauthorize-success':          // checkin record created but queue not deleted
                        console.log('Server return status: '+ data.status+', '+data.message);  //  checkin_create_exception
                        setTimeout(function(){ navigator.notification.alert(data.message); }, 3000);   // data.bookingData.response_text
                        
                        service.room.walletRegDeviceId(data.data);
                    //    service.room.walletRegDeviceToken(data.data.legicWalletToken);
                         /*  service.room.walletRegDeviceId(function(data.data.legicCustomNumber){ 
                               console.log('Custon number register success!');
                               service.room.finishRegistration(function(data.data.legicWalletToken) { 
                                   console.log('Legic token register success!');
                               }, function (err) {
                                   console.error('error registering token:', err);
                               });
                           }, function (err) {
                               console.error('error registering custom number:', err);
                           });   */
   
                        break;
                        
                    case 'booking_success':          // checkin record created but queue not deleted
                        console.log('Server return status: '+ data.status+', '+data.message);  //  checkin_create_exception
                        setTimeout(function(){ navigator.notification.alert(data.message); }, 3000);   // data.bookingData.response_text
                        service.room.legicKeyServerSync();
                        break;
                        
                        
                    default:
                        navigator.notification.alert('No service match. Server return status is: '+ data.status+', '+data.message);
                }
                        if ($.isEmptyObject(currentQueue)) console.log('Queue cleared from client cache')
                        else console.log('Queue in client cache')
                
            }
                
},
    
checkinServices = {
    
    manualCheckin: function() {
        
    },
    
    
    
    
},
    
    
    
    
    
 /*
*
*
* CONTACTS CONTROLLER
*
*
*/ 
   
 /*  var Contacts = {
       
       beerFilterId : null,

       init: function() { 
          
           this.displayList();
           
           $("#btnImportContacts").bind("tap", function(e) {
              if (navigator.contacts) {
                  var friendName = window.prompt("Enter your friend's name","");
                  if (friendName != null) {
                    Contacts.importContacts(friendName);
                  }
              } else {
                navigator.notification.alert("Not supported on web platform");   
              }
           });
           
           $("#btnAdd").bind("tap", function(e) {
               ContactForm.friendId = null;
           });
            
       },
       
       displayList: function() {
         
         var list = $('#friendsList > ul');
         
         var friendTemplate = $("#tplFriend").html();
       
         list.empty();
        
         // dynamic where clause for beer id
         var sql = ''.concat(
          'select * ',
          'from friend '
         );
         if (this.beerFilterId != null) {
          sql+= "where beerid = " + this.beerFilterId + " ";
         }
         sql+= " order by lastName,firstName";
       
        
         CloudtraqObjectsDB.runQuery(sql, function(records) {
            for (var i=0; i<records.length; i++) {
             var item = records[i];
             list.append(friendTemplate.format(item.id, item.lastName, item.firstName));
            }
            list.listview("refresh");
             
            // attach event listeners
            $('#friendsList li').bind('tap', function(e) {
                ContactForm.friendId = this.getAttribute('data-value');
                ContactDetail.friendId = ContactForm.friendId;
                $.mobile.changePage('contactdetail.html');
            });
         });      
       },
       
       importContacts: function(friendName) {
         $.mobile.loading( 'show', {
           text: "Please Wait",
           textVisible: true,
           theme: "a",
           textonly: false,
           html: ""
         });
         
         var options = new ContactFindOptions();
         
         options.filter=friendName; 
         options.multiple=true;
         
         var fields = [
              "displayName",   
              "name",
              "addresses",
              "phoneNumbers",
              "emails"
         ];
         navigator.contacts.find(fields, this.onImport, this.onImportError, options);
       },
       
       getPreferred: function(arr) {

           for (var i=0; i<arr.length; i++) {
             if (arr[i].pref) {
              return arr[i];   
             }
           }
           if (arr.length > 0) {
             return arr[0]
           } else {
             return null;
           }
       },
       
       onImport: function(contacts) {
        var me = this;
        $.mobile.loading( "hide" );
        var address= null, phone = null, email = null, count = 0;
        for (var i=0; i<contacts.length; i++) {
            if (contacts[i].name.givenName != undefined) {
              count++;
              var friend = [
                  {
                    name: "firstName",
                    value: new String(contacts[i].name.givenName)
                  },
                  {
                    name: "lastName",
                    value: new String(contacts[i].name.familyName)
                  }
              ];
              
              if (contacts[i].addresses) {
                address = Contacts.getPreferred(contacts[i].addresses);
                friend.push({ name: "address", value: address.streetAddress});
                friend.push({ name: "zipcode", value: address.postalCode});
              }   
            
              if (contacts[i].phoneNumbers) {
                 friend.push({ name: "phone", value: Contacts.getPreferred(contacts[i].phoneNumbers).value});
              }

              if (contacts[i].emails) {
               friend.push({ name: "email", value: Contacts.getPreferred(contacts[i].emails).value}); 
              }
            
              CloudtraqObjectsDB.writeRecord('friend',null, friend);
          }
        }
        navigator.notification.alert(count + " Contacts Imported");
        setTimeout("Contacts.displayList()",500);
       },
       onImportError: function(err) {
         $.mobile.loading( "hide" );
         navigator.notification.alert("Import Failed");   
       }
   };   */
    
    
    
    
    
    
    
    
    
    /*
*
*
* CONTACTFORM CONTROLLER
*
*
*/ 
   
ContactForm = {

         friendId: null,
         userName: null, 
         deviceUUID: null,

         init: function() {

           this.initialized=true;
           this.populateBeerField();
           this.initValidation();
           this.loadRecord();
           var me = this;

           $("#btnSave").bind("tap", function(e) {
              me.submitForm();
           });

            $("#btnDeleteRecord").bind("tap", function(e) { // bind delete contact event to btnDeleteRecord
                me.deleteRecord();  
            }); 

           $("#address").bind("change", this.geoCode);
           $("#zipcode").bind("change", this.geoCode);

         },

        loadRecord: function() {
            var me = this;
            var table = "friend";
            var cond = "id = '" + me.friendId+"'";
            $("#btnDeleteRecord").fadeIn();
            $("div#friendsHeader > h1").html("Edit Contact"); 
            if(this.friendId !== null) {
                if(this.friendId === 0) {  // edit friend record  cond = id = " + this.friendId;
                    table = "profile";
                    cond = "userName = '" + App.USERNAME+"'";
                    $("#btnDeleteRecord").hide();
                    $("div#friendsHeader > h1").html("Edit Profile");
                }
                var sql = "select * from " + table + " where " + cond; 
                CloudtraqObjectsDB.runQuery(sql, function(records) {

                    console.log("records: " + records);
                    var rec = records[0];
                    for(var i in rec) {
                           $('#' + i).val(rec[i]);
                        console.log('i in loadRecord is : ' + i);
                    }
                    $("#beerId").selectmenu("refresh",true);
                });
            } else {  // create new freind record
                $("#btnDeleteRecord").hide();
                $("form#contactform").trigger('reset'); //jquery reset the form fields
                $("div#friendsHeader > h1").html("Add Contact");
            }
          },   

        // Delete contact from friend table
        deleteRecord: function() {
            var doDeleteRecord = navigator.notification.confirm("Delete contact?"); 
            if (doDeleteRecord) {
                var sql = "DELETE FROM friend WHERE id=" + ContactForm.friendId;  //AND firstName='Maria Anders'
             //   navigator.notification.alert("ContactForm.friendId = " + ContactForm.friendId);
                CloudtraqObjectsDB.runQuery(sql, function(records) {
                    if(records) {
                        navigator.notification.alert('Contact deleted');
                        $.mobile.changePage('contacts.html');
                    }
                    else {
                        navigator.notification.alert('Contact delete error!');
                    }
                });
            }
            else {
                navigator.notification.alert('Canceling delete contact');
            } 
        }, 

         populateBeerField: function() {
           var beerField = $("select#beerId");
           // beerField.empty();
           for (var i=0; i< App.beers.length; i++) {
            beerField.append(
                '<option value="{0}">{1}</option>'.format(App.beers[i].id,App.beers[i].name)
            );
           }
           beerField.selectmenu("refresh", true);   
         },    

         initValidation: function() {
          var form = $("form#contactform");
          form.validate({
            submitHandler: function(form) {
                // $(form).ajaxSubmit();
                navigator.notification.alert(2);
            },
            rules: {
             lastName: {
               required: true,
               rangelength: [2,20]
             },
             firstName: {
              required: true
             },
             address: {
              required: true
             },
             zipcode: {
              required: true,
              digits: true
             }
          }
         });
        },

        submitForm: function() {

           var form =  $("form#contactform");
           var validator = form.validate();
           validator.form();
           if (validator.numberOfInvalids() === 0) {

               var fields = form.serializeArray();

               fields[fields.length] = {
                 name: 'lat',
                 value: $('#lat').val()
               };

               fields[fields.length] = {
                 name: 'lng',
                 value: $('#lng').val()
               };

          /*     fields[fields.length] = {
                 name: 'userName',
                 value: null
               };

               fields[fields.length] = {
                 name: 'deviceUUID',
                 value: null
               }; */
               if(ContactForm.friendId === 0) {
               //    App.UPDATEPROFILE = 1;
                //    App.PROFILEMODS = form.serialize();
                //   navigator.notification.alert("PROFILEMODS are: " + App.PROFILEMODS);
                //   systemLogin.ServerLogIn();  // with UPDATEPROFILE = 1 will update server profild
                //   systemLogin.GetUserAccountProfile();  // gets from server and loads in websql
                   var myProfileRecord = 1;
                   CloudtraqObjectsDB.writeRecord('profile', myProfileRecord, fields, function() {

                        navigator.notification.alert(" Profile Record Updated");
                        $.mobile.changePage('index.html');
                   });  

               } else {
                    CloudtraqObjectsDB.writeRecord('friend', this.friendId, fields, function() {
                        navigator.notification.alert("Friend Record Saved");
                        $.mobile.changePage('contacts.html');
                    }); 
               }
           }
        },

        geoCode: function() {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                address : $('#address').val() + " " + $('#zipcode').val()
             }, function(results,status) {
                if(status != google.maps.GeocoderStatus.OK) {
                 navigator.notification.alert("Address not found");
                 $("#lat").val('');
                 $("#lng").val('');
               } else {					
                 $("#lat").val(results[0].geometry.location.lat()); 
                 $("#lng").val(results[0].geometry.location.lng()); 
               }
             });
         }

};