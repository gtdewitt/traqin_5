// JavaScript Document
var PropertyViewObjectsDB =  {
    db: null,
    init: function () {
        var me = this;
        
        // allocate a 5mb database
        this.db = openDatabase('PropertyViewObjects', '1.0', 'Sensor Node & Mobile Device Cached Data', 5 * 1024 * 1024);
        
        // drop a table
    /*   this.db.transaction(function(tx) {
            tx.executeSql('DROP TABLE chat', [], me.onSuccess, me.onError);
        });   */
        
       // create the tables
    /*     this.db.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS beer (id INTEGER, beerid TEXT, name TEXT, type TEXT, country TEXT, status TEXT)', [], me.onSuccess, me.onError);
        });  */
        
         // create the tables
        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS beer (id INTEGER, name TEXT, type TEXT, country TEXT)', [], me.onSuccess, me.onError);
        });

        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS friend (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName TEXT, lastName TEXT, address TEXT, zipcode TEXT, email TEXT, phone TEXT, lat TEXT, lng TEXT, beerId INTEGER, userName TEXT, deviceUUID TEXT)', [], me.onSuccess, me.onError);
        });       
        
         this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS profile (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName TEXT, lastName TEXT, userName TEXT, address TEXT, zipcode TEXT, email TEXT, phone TEXT, deviceName TEXT, deviceModel TEXT, deviceCordova TEXT, devicePlatform TEXT, deviceUUID TEXT, deviceVersion TEXT, lat TEXT, lng TEXT, priv TEXT, loggedIn INTEGER, wloggedIn INTEGER, latest INTEGER, latestLoc INTEGER, btMac TEXT, beerId INTEGER)', [], me.onSuccess, me.onError);      
             
                  /*        $output = '{"firstName":"' . $fname . '","lastName":"' . $lname . '", "userName":"' . $uname . '", "address" :"' . $address . '" , "zipcode" :"' . $zipcode . '" , "email" :"' . $email . '" , "phone" :"' . $cellnum . '" , "deviceName" :"' . $dname . '" , "deviceModel" :"' . $dmodel . '" , "deviceCordova" :"' . $dcordova . '" , "devicePlatform" :"' . $dplatform . '" , "deviceUUID" :"' . $duuid . '" , "deviceVersion" :"' . $dversion . '" , "lat" :"' . $lat . '" , "lng" :"' . $lng . '" , "priv" :"' . $priv . '" , "loggedIn" :"' . $lgdin . '" , "wloggedIn" :"' . $wlgdin . '" , "latest" :"' . $latest . '" , "latestLoc" :"' . $latestloc . '" , "btMac" :"' . $bt_mac . '" , "beerId" :"' . $beerId . '"   }';  user profile data  */
             
                 /*  tx.executeSql('CREATE TABLE IF NOT EXISTS profile (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName TEXT, lastName TEXT, address TEXT, zipcode TEXT, email TEXT, phone TEXT, lat TEXT, lng TEXT, beerId INTEGER, userName TEXT, deviceUUID TEXT, deviceName TEXT, deviceModel TEXT, deviceCordova TEXT, devicePlatform TEXT, deviceVersion TEXT, priv TEXT, loggedIn INTEGER, wloggedIn INTEGER, latest INTEGER, latestLoc INTEGER, btMac TEXT)', [], me.onSuccess, me.onError);
                 
                 $output = '{"firstName":"' . $fname . '","lastName":"' . $lname . '",  "address" :"' . $address . '" , "zipcode" :"' . $zipcode . '" , "email" :"' . $email . '" , "phone" :"' . $cellnum . '" , "lat" :"' . $lat . '" , "lng" :"' . $lng . '" , "beerId" :"' . $beerId . '" , "userName":"' . $uname . '" , "deviceUUID" :"' . $duuid . '" , "deviceName" :"' . $dname . '" , "deviceModel" :"' . $dmodel . '" , "deviceCordova" :"' . $dcordova . '" , "devicePlatform" :"' . $dplatform . '"  , "deviceVersion" :"' . $dversion . '" , "priv" :"' . $priv . '" , "loggedIn" :"' . $lgdin . '" , "wloggedIn" :"' . $wlgdin . '" , "latest" :"' . $latest . '" , "latestLoc" :"' . $latestloc . '" , "btMac" :"' . $bt_mac . '"}';    */
                          
        });      
        
        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS chat (num INTEGER, sender TEXT, recip TEXT, message TEXT)', [], me.onSuccess, me.onError);
        });

        // create the tables
        
     //   {building:[{'groundOverlay':'Micrclient_HQ_8.png', 'cntrLat':'36.038868', 'cntrLng':'-115.086716', 'swLat':'36.038811', 'swLng':'-115.086792', 'neLat':'36.038925', 'neLng':'-115.086655'}]}

        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS floormap (groundOverlay TEXT, cntrLat INTEGER, cntrLng INTEGER, swLat INTEGER, swLng INTEGER, neLat INTEGER, neLng INTEGER)', [], me.onSuccess, me.onError);
        });
        
        /* Datamodel     App.NODES[
                                ang: "213.94"
                                dist: "9.32"
                                image: "sensepoint_tri_node.png"
                                lat: "36.038826"
                                lng: "-115.086784"
                                maxNodes: "5"
                                nodeID: "node004"
                                nodeType: "sensePoint"]; */
        
        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS hubs (nodeID TEXT, nodeType TEXT, maxNodes INTEGER, image TEXT, lat INTEGER, lng INTEGER, ang INTEGER, dist INTEGER)', [], me.onSuccess, me.onError);
        });
        
        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS nodes (nodeID TEXT, nodeType TEXT, maxNodes INTEGER, image TEXT, lat INTEGER, lng INTEGER, ang INTEGER, dist INTEGER)', [], me.onSuccess, me.onError);
        });
        
        /* Datamodel     App.BLUEDOTS[
                                btMac: "d0:37:61:c4:86:2a"
                                cod: "001f00"
                                frameID: "10551"
                                name: "metawatch"
                                nodeID: "node002"
                                numrows: "10"
                                rssi: "-73"
                                timeStamp: "2012-05-22 23:04:18"
                                username: "Mentor"];  */
        
        /*	Bluetooth Scan Frame Sample	 
	$row[0]				$row[1]			  $row[2]	$row[3] $row[4]			$row[5]         $row[6]
   Frame ID			Time Stamp			Node ID		COD		RSSI		MAC Address		Friendly Name
	1264		2012-06-29 22:16:16		node003		001f00	-54		00:07:80:4b:2b:cd		wt12-a		*/ 
		 
// $query = "SELECT * FROM bluetoothData WHERE $cond (ID_frame > 0) ORDER BY ID_frame DESC LIMIT 50";
// $query = "SELECT * FROM bluetoothData WHERE $cond ORDER BY ID_frame $max";

        this.db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS bluedots (frameID INTEGER, username TEXT, numrows INTEGER, timeStamp TEXT, nodeID TEXT, cod TEXT, rssi INTEGER, btMac TEXT, name TEXT)', [], me.onSuccess, me.onError);
        }); 

        return this.db;
    }, 

   runQuery : function(sql, callback) {
        this.db.transaction(function (tx) {
            tx.executeSql(sql, 
                          [],
                          function(tx,rs) {
                            var a = [];
                            for (var i=0; i < rs.rows.length; i++) {
                                a[a.length] = rs.rows.item(i);
                            }
                            callback(a);
                          },
                          function() {             
                            console.log('fail',arguments);
                          }
            )
        });
    },

    writeRecord : function(tablename,id,fields,callback) {
        
       
        if (id == null || id == "") {
            // insert
            var aFields = [], aPlaceHolders = [], aValues=[];
            for (var i=0; i<fields.length; i++) {
               aFields.push(fields[i].name);
               aValues.push(fields[i].value);
               aPlaceHolders.push('?');
            }
            var sql = 'INSERT INTO ' + tablename + ' (' + aFields.join(',') + ') VALUES (' + aPlaceHolders.join(',') + ')';
            

            this.db.transaction(function (tx) {
                tx.executeSql(sql, 
                              aValues,
                              function() {
                                if (callback)
                                 callback(arguments);
                              },
                              function() {
                                alert('db insertion error');
                              })
            });

        } else {
            var sql = "update " + tablename + " set ";
            var aValues = [];
            for (var i=0; i<fields.length; i++) {
                sql+= fields[i].name + " = ?";
                aValues.push(fields[i].value);
                if (i < fields.length - 1)
                    sql += ', '
            }
            sql += ' where id = ?'
            aValues.push(id);
            this.db.transaction(function (tx) {
                tx.executeSql(
                    sql,
                    aValues,
                    function() {
                        callback(arguments);
                    },
                    function() {
                        console.log('db insertion error', arguments);
                    }
                )
            });


        }

    },

   importTable : function (tablename, aData) {

        this.db.transaction(function (tx) {
            tx.executeSql('delete from ' + tablename);
        });

        if (aData.length > 0) {

            // build SQL expression   
            var aFields = [],
                aPlaceHolders = [];
            for (var key in aData[0]) {
                aFields.push(key);
                aPlaceHolders.push('?');
            }
            var sql = 'INSERT INTO ' + tablename + ' (' + aFields.join(',') + ') VALUES (' + aPlaceHolders.join(',') + ')';

            // write data to db
            this.db.transaction(function (tx) {

                for (var i = 0; i < aData.length; i++) {
                    var aValues = [];

                    for (var key in aData[i]) {
                        aValues.push(aData[i][key]);
                    }

                    tx.executeSql(sql,
                    aValues,

                    function () {
                        console.log('inserted row')
                    },

                    function () {
                        console.log('failed', arguments)
                    });
                }
            });
        }
    },

    onError : function (tx, e) {
        alert("Error: " + e.message);
    },
    
    onSuccess : function (e) {
        console.log('dbsuccess', e);
    }
}

PropertyViewObjectsDB.init();


 /* {
   "sensePoint": [
      {
         "maxNodes": 5,
         "name": "meshPoint Node0",
         "image": "ct_marker_shdw_64.png",
         "lat": 36.038895,
         "lng": -115.08672,
         "dist": "0",
         "ang": "reference"
      },
      {
         "maxNodes": 5,
         "name": "sensePoint Node1",
         "image": "sensepoint_tri_node.png",
         "lat": 36.03893,
         "lng": -115.086784,
         "dist": "6.61",
         "ang": "303.41"
      },
      {
         "maxNodes": 5,
         "name": "sensePoint Node2",
         "image": "sensepoint_tri_node.png",
         "lat": 36.038925,
         "lng": -115.08666,
         "dist": "5.99",
         "ang": "60.83"
      },
      {
         "maxNodes": 5,
         "name": "sensePoint Node3",
         "image": "sensepoint_tri_node.png",
         "lat": 36.038864,
         "lng": -115.0867,
         "dist": "4.23",
         "ang": "148.3"
      },
      {
         "maxNodes": 5,
         "name": "sensePoint Node4",
         "image": "sensepoint_tri_node.png",
         "lat": 36.038826,
         "lng": -115.086784,
         "dist": "9.32",
         "ang": "213.94"
      }
   ]
}  */