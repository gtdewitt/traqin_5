

    // start login after deviceready        
        
    Login = {
        // Global Variable declaration

     //   RESPONCE:   null,
     //   LOGGEDIN: false,
        
        // Login initiatlization methods
        init: function() {
            this.checkPreAuth();
            $("#loginForm").on("submit",Login.handleLogin);
        },

        // pre authentication check
        checkPreAuth: function() {
            var form = $("#loginForm");
            if(window.localStorage["username"] != undefined && window.localStorage["password"] != undefined) {
                $("#username", form).val(window.localStorage["username"]);
                $("#password", form).val(window.localStorage["password"]);
            }
        },

        //Login services
        handleLogin: function() {
            console.log('Im in Jimmy');
            var form = $("#loginForm");
            //disable the button so we can't resubmit while we wait
            $("#submitButton",form).attr("disabled","disabled");
            var un = $("#username", form).val();
            var pw = $("#password", form).val();
            console.log("click");
            var loginURI = dataSource.meshPoint.BASEURL + dataSource.meshPoint.GETLOGIN; // Server PHP MySQL login
          //  console.log('Login data is ' + loginURI);
            if(un != '' && pw!= '') {
                $.post(loginURI, {u:un,p:pw}, function(data,status) {
                //    Login.RESPONCE = data;
                    if(status == 'success') {
                        //store
                        if(data.Status == 'Valid'){
                            window.localStorage["username"] = un;
                            dataSource.guestInfo.USERNAME = un;
                            window.localStorage["password"] = pw;
                            dataSource.guestInfo.PASSWORD = pw;
                            dataSource.guestInfo.ARGS = 'u=' + un + '&p=' + pw;
                            Login.GetUserAccountProfile(un,pw);
                            dataSource.guestInfo.LOGGEDIN = true;
                        //    $("#serviceBtns").fadeIn();
                        //    $("#loginBtns").hide();
                        //    $.mobile.changePage("#loginPage");
                            appPage.listView();
                        }
                    } else {
                        alert("Your login failed");
                      //  navigator.notification.alert("Your login failed", function() {});
                    }
                    $("#submitButton").removeAttr("disabled");
                },"json"); 
            } else {
                //Thanks Igor!
                alert("You must enter a username and password");
               // navigator.notification.alert("You must enter a username and password", function() {});
                $("#submitButton").removeAttr("disabled");
            }
        return false;
        },
        
        GetUserAccountProfile: function(username,password) {
            var getUserUrl = dataSource.meshPoint.BASEURL + dataSource.meshPoint.GETUSERDATA;   // Get user profile from server
            var args = 'u=' + username + '&p=' + password + '&ap=1';
               $.post(getUserUrl, args , function(data, status){

                if (status == 'success') {
                    if (data.length > 0) {
                        var obj = JSON.parse(data);
                        var userProfile = obj.user;    
                       console.log("User profile data is: " + data);
                        console.log("useProfile is: " + userProfile);
                        PropertyViewObjectsDB.importTable('profile',userProfile);
                        dataSource.meshPoint.USERPROFILE = userProfile;
                        userProfile = null;
                    } else {
                       alert("No profile");
                    } 
                } else {
                     alert("Status: " + status);
                }  
               });  
        }
        
    };
    