package com.legic.mobile.app.sdk_quickstart.util;

public enum SaflokLockError {

    // Saflok Errors

     Error113(113,"All Keys: The key is not valid in the current lock mode or cycle. Lock is typically in an invalid mode. This code may also appear if you use standard or display keys when the PPK or SPK LEDs are flashing. "),
     Error117(117,"Standard, Special, PPK and SPK Keys: The lock is in shutdown cycle because 10 or more invalid keys were used. A valid key must be used to terminate shutdown mode. "),
     Error122(122,"Standard Keys: Lock level is non-operating for this key’s type. The key does not belong to this lock. "),
     Error123(123,"Standard and Special Keys: Key is not mastered into the lock. The key does not belong to this lock. "),
     Error127(127,"Standard Keys: Key does not have one of the 12 valid pass numbers required for the pass level. "),
     Error128(128,"Standard and Special Keys: Key is an old key; its new key date is earlier than the lock’s key data. Replace the key with a current or new key. "),
     Error129(129,"Standard and Special Keys: Key is an old key; its new key time is earlier than the lock’s key data. Replace the key with a current or new key. "),
     Error134(134,"Standard and Special Keys: Key is an old key, level has limited sequence range (1-15) but key’s sequence number is older than the lock’s key data. Replace key with a current or new key. Use a re-sequence key if necessary. "),
     Error135(135,"Standard and Special Keys: Key is too new, level has limited sequence range and key’s sequence number is beyond the lock’s key data and range. Make and use a re-sequence key, then try the original key again. "),
     Error136(136,"Standard Key: Key is a limited use key and has already been used the maximum number of times. Replace key with a new key. "),
     Error137(137,"Standard Key: The key is a new key but the door is dead bolted and the new key cannot be accepted. If key is for a new guest, the room is still occupied. Assign new guest to another room. "),
     Error141(141,"All Keys: The key is expired based on the lock’s clock date and time. Replace key with a duplicate key with a later expiration date and time. "),
     Error149(149,"Standard Key: Key is current but key’s level is inhibited. Replace key with a new key. "),
     Error150(150,"Special Key: Key is current but key is inhibited. Replace key with a new key. "),
     Error152(152,"Standard Key: Key’s level is electronically locked out. If this key is a dual, electronic lock/unlock, inhibit level, or unlatch/latch key; it will not perform its function. The electronic unlock key must be used to remove the electronic lockout. "),
     Error154(154,"Standard Key: Key is an opening key, but the lock is dead bolted and the key cannot override it. "),
     Error178(178,"Standard Key: The key is new and is identified as a pre-reg key which will not work until its check-in date and time. If desired, replace key with a new key. "),
     Error255(255,"Generic Error ");
    
    

    private final int code;
    private final String description;

    SaflokLockError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static SaflokLockError fromCode(int code) {
        for (SaflokLockError error : values()) {
            if (error.getCode() == code) {
                return error;
            }
        }
        return Error255;
    }
}